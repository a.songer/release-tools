namespace :security do
  # Undocumented; should be a pre-requisite for every task in this namespace!
  task :force_security do
    unless ReleaseTools::SharedStatus.critical_security_release?
      ENV['SECURITY'] = 'true'
    end
  end

  desc 'Create a patch release task issue'
  task issue: :force_security do |_t, args|
    issue = ReleaseTools::PatchRelease::SecurityIssue.new(versions: args[:versions])

    create_or_show_issue(issue)
  end

  desc 'Merges valid security merge requests'
  task :merge, [:merge_default] => :force_security do |_t, args|
    merge_default =
      if args[:merge_default] && !args[:merge_default].empty?
        true
      else
        false
      end

    ReleaseTools::Security::MergeRequestsMerger
      .new(merge_default: merge_default)
      .execute
  end

  desc 'Toggle the security merge train based on need'
  task :merge_train do |_t, _args|
    ReleaseTools::Security::MergeTrainService
      .new
      .execute
  end

  desc 'Prepare for a new patch release'
  task prepare: :force_security do |_t, _args|
    issue_task = Rake::Task['security:issue']
    issue_task.execute
  end

  desc 'Publish the current patch release'
  task :publish, [:version] => :force_security do |_t, args|
    $stdout
      .puts "Using security repository only!\n"
      .colorize(:red)

    publish_task = Rake::Task['publish']
    publish_task.execute(version: args[:version])
  end

  desc "Check a patch release's build status"
  task status: :force_security do |t, _args|
    status = ReleaseTools::BranchStatus.for_security_release

    status.each_pair do |project, results|
      results.each do |result|
        ReleaseTools.logger.tagged(t.name) do
          ReleaseTools.logger.info(project, result.to_h)
        end
      end
    end

    ReleaseTools::Slack::ChatopsNotification.branch_status(status)
  end

  desc 'Create child jobs for tagging patch releases'
  task tag_jobs: :force_security do
    versions = ENV['VERSIONS'].split(',')
    tag_jobs = Security::TagJobs.new(versions).generate
    File.write('dynamic-tag-gitlab-ci.yml', tag_jobs)
  end

  desc 'Tag a new patch release'
  task :tag, [:version] => :force_security do |_t, args|
    ReleaseTools::Security::TagPatchRelease
    .new(args[:version])
    .execute
  end

  desc 'Validates merge requests in security projects'
  task validate: :force_security do
    ReleaseTools::Security::ProjectsValidator
      .new(ReleaseTools::Security::Client.new)
      .execute
  end

  desc 'Sync default and auto-deploy branches'
  task sync_remotes: :force_security do
    ReleaseTools::Security::SyncRemotesService
      .new
      .execute
  end

  desc 'Sync Git tags'
  task :sync_git_tags, [:git_versions] => :force_security do |_t, args|
    git_versions = args[:git_versions].split

    ReleaseTools::Security::SyncGitRemotesService
      .new(git_versions)
      .execute
  end

  namespace :gitaly do
    desc 'Tag a new Gitaly security release'
    task :tag, [:version] => :force_security do |_, args|
      Rake::Task['release:gitaly:tag'].invoke(*args)
    end
  end

  desc 'Link/Unlink security-target issues for a patch release'
  task process_security_target_issues: :force_security do
    ReleaseTools::Security::TargetIssuesProcessor.new.execute
  end

  namespace :prepare do
    desc 'Notify the prepare pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :start, release_type: :patch)
        .execute
    end

    desc 'Notify JiHu about the patch release'
    task notify_jihu: :force_security do
      ReleaseTools::Security::Prepare::NotifyJihu.new.execute
    end

    desc 'Notify a stage team about the patch release'
    task notify_stage_team: :force_security do
      team = ENV.fetch('STAGE_TEAM', nil).to_sym

      ReleaseTools::Slack::Security::TeamNotifier
        .new(team: team)
        .execute
    end

    desc 'Check components for green pipelines'
    task check_component_branch_pipeline_status: :force_security do
      ReleaseTools::Security::Prepare::ComponentBranchVerifier.new.execute
    end

    desc 'Reviews if the patch release includes security fixes'
    task review_security_fixes: :force_security do
      ReleaseTools::Security::Prepare::FixesVerifier.new.execute
    end

    desc 'Create AppSec Issue'
    task appsec_issue: :force_security do
      issue = ReleaseTools::Security::AppSecIssue.new

      create_or_show_issue(issue)
    end

    desc 'Create Comms Issue'
    task comms_issue: :force_security do
      issue = ReleaseTools::Security::CommsTaskIssue.new

      create_or_show_issue(issue)
    end
  end

  namespace :release_preparation do
    desc 'Notify the release preparation steps have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :release_preparation, release_type: :patch)
        .execute
    end
  end

  namespace :early_merge do
    desc 'Notify the merging steps for the security default branch MRs have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :early_merge, release_type: :patch)
        .execute
    end
  end

  namespace :backport_merge do
    desc 'Notify the merging steps for the security stable branches MRs have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :backport_merge, release_type: :patch)
        .execute
    end
  end

  namespace :tag do
    desc 'Notify the tagging steps for the patch release versions have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :tag, release_type: :patch)
        .execute
    end
  end

  namespace :publish do
    desc 'Notify the release preparation steps have started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :publish, release_type: :patch)
        .execute
    end

    desc 'Move the blog post from security to canonical'
    task move_blog_post: :force_security do
      ReleaseTools::Security::Publish::MoveBlogPost.new.execute
    end

    desc 'Deploy the patch release blog post'
    task deploy_blog_post: :force_security do
      ReleaseTools::Security::Publish::DeployBlogPost.new.execute
    end
  end

  namespace :finalize do
    desc 'Notify the finalize pipeline has started'
    task start: :force_security do
      ReleaseTools::Slack::ReleasePipelineStartNotifier
        .new(stage: :finalize, release_type: :patch)
        .execute
    end

    desc 'Close implementation issues'
    task close_issues: :force_security do
      ReleaseTools::Security::Finalize::CloseImplementationIssues
        .new
        .execute
    end

    desc 'Notify the completion of the patch release'
    task notify_release: :force_security do
      ReleaseTools::Security::Finalize::NotifyReleaseComplete.new.execute
    end

    desc 'Enable Gitaly Update Task'
    task enable_gitaly_update_task: :force_security do
      ReleaseTools::Security::Finalize::GitalyUpdateTask
        .new(action: :enable)
        .execute
    end

    desc 'Check tags are synced to canonical'
    task check_canonical_tags_synced: :force_security do
      ReleaseTools::Security::Finalize::CheckCanonicalTagsSynced.new.execute
    end

    desc 'Update Security Tracking Issue'
    task update_tracking_issue: :force_security do
      updater = ReleaseTools::Security::Finalize::CloseTrackingIssue.new
      updater.execute

      security_tracking_issue = ReleaseTools::Security::TrackingIssue.new
      create_or_show_issue(security_tracking_issue)
    end

    desc 'Enable the security-target issue processor'
    task enable_security_target_processor: :force_security do
      ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor.new.execute(action: :enable)
    end

    desc 'Notify upcoming release managers'
    task notify_upcoming_release_managers: :force_security do
      ReleaseTools::Security::Finalize::NotifyNextReleaseManagers.new.execute
    end

    desc 'Update the slack bookmark of the security tracking issue'
    task update_slack_bookmark: :force_security do
      ReleaseTools::Security::Finalize::UpdateSlackBookmark
        .new.execute
    end

    desc 'Create a new patch release status metric'
    task create_release_status_metric: :force_security do
      ReleaseTools::Metrics::PatchReleaseStatus
        .new(status: :open)
        .execute
    end
  end
end
