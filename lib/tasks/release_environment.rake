# frozen_string_literal: true

namespace :release_environment do
  desc 'Create a release environment'
  task :create, [:version] do |_t, args|
    version = args[:version]

    ReleaseTools::Tasks::ReleaseEnvironment::Environment
      .new(version)
      .execute
  end

  desc 'Send Slack message about the state of the release environment deployment result'
  task :notify, [:pipeline_id, :environment_name, :release_environment_version] do |_t, args|
    pipeline_id = args[:pipeline_id]
    environment_name = args[:environment_name]
    release_environment_version = args[:release_environment_version]

    ReleaseTools::Tasks::ReleaseEnvironment::DeployNotifier.new(
      pipeline_id: pipeline_id,
      environment_name: environment_name,
      release_environment_version: release_environment_version
    ).execute
  end

  desc 'Send Slack message when release environment QA fails'
  task :qa_notify, [:pipeline_id, :environment_name, :release_environment_version] do |_t, args|
    pipeline_id = args[:pipeline_id]
    environment_name = args[:environment_name]
    release_environment_version = args[:release_environment_version]

    ReleaseTools::Tasks::ReleaseEnvironment::QaNotifier.new(
      pipeline_id: pipeline_id,
      environment_name: environment_name,
      release_environment_version: release_environment_version
    ).execute
  end
end
