# frozen_string_literal: true

namespace :post_deploy_migrations do
  desc 'Sends a Slack notification about the status of post-deployment migrations'
  task :notify do
    ReleaseTools::AutoDeploy::PostDeployMigrations::Notifier.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID', nil),
      environment: ENV.fetch('DEPLOY_ENVIRONMENT', nil)
    ).execute
  end

  desc 'Sends a Slack notification about the status of QA on post-deploy migration pipeline'
  task :qa_notify do
    ReleaseTools::AutoDeploy::PostDeployMigrations::Qa::Notifier.new(
      pipeline_id: ENV.fetch('CI_PIPELINE_ID', nil),
      environment: ENV.fetch('DEPLOY_ENVIRONMENT', nil)
    ).execute
  end

  desc 'Performs production checks before executing the post-deployment migrations, and posts note to monthly release issue'
  task :prepare do
    ReleaseTools::AutoDeploy::PostDeployMigrations::Prepare.new.execute
  end

  desc 'Builds post-migrations artifacts'
  task :build_post_migrations_artifact do
    ReleaseTools::AutoDeploy::PostDeployMigrations::Artifact.new.create
  end

  desc 'Track a deployment on the post-deploy environments using the GitLab API'
  task :track_deployment do
    environment = ENV.fetch('DEPLOY_ENVIRONMENT')
    status = ENV.fetch('DEPLOY_STATUS')

    # environment will be 'db/gstg' or 'db/gprd'
    version = current_auto_deploy_package_on_environment(environment.split('/').last)

    meta = ReleaseTools::Deployments::Metadata.new(version)

    ReleaseTools::SharedStatus.as_security_release(meta.security_release?) do
      tracker = ReleaseTools::AutoDeploy::PostDeployMigrations::DeploymentTracker
        .new(environment: environment, status: status, version: version)

      ReleaseTools.logger.info(
        'Starting product deployment tracker for post-migrations',
        env: environment,
        status: status,
        version: version,
        dry_run: dry_run?
      )

      tracker.record_metadata_deployment

      ReleaseTools.logger.info(
        'Starting deployment tracker for post-migrations',
        env: environment,
        status: status,
        version: version,
        dry_run: dry_run?
      )

      deployments = tracker.track

      ReleaseTools.logger.info(
        'Adding post-migration labels',
        env: environment,
        dry_run: dry_run?
      )

      if status == 'success'
        ReleaseTools::ErrorTracking.with_exception_captured do
          ReleaseTools::AutoDeploy::PostDeployMigrations::MergeRequestLabeler
            .new
            .label_merge_requests(environment, deployments)
        end
      end

      ReleaseTools::Deployments::SentryTracker.new.deploy(environment, status, version)
    end
  end
end
