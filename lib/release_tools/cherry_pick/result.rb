# frozen_string_literal: true

module ReleaseTools
  module CherryPick
    # Represents the result of a cherry pick
    class Result
      attr_reader :merge_request, :reason, :new_sha

      # merge_request - The merge request we attempted to pick
      # status        - Status of the pick (`:success`, `:failure`, `:denied`, `:not_required`)
      # new_sha       - The SHA returned by the cherry-pick API.
      def initialize(merge_request:, status:, reason: nil, new_sha: nil)
        @merge_request = merge_request
        @status = status
        @reason = reason
        @new_sha = new_sha
      end

      def success?
        @status == :success
      end

      def denied?
        @status == :denied
      end

      def not_required?
        @status == :not_required
      end

      def failure?
        !success?
      end

      def title
        merge_request.title
      end

      def url
        merge_request.web_url
      end

      def to_markdown
        "[#{title}](#{url})"
      end
    end
  end
end
