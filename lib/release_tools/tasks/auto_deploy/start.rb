# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Start
        include ::SemanticLogger::Loggable

        def initialize(product_version: ProductVersion.last_auto_deploy)
          @product_version = product_version
        end

        def execute
          logger.info('Preparing for rollout', product_version: product_version.version, deploy_version: deploy_version, gitlab_sha: commit)

          store_deploy_version
          sentry_release

          notify
        end

        private

        attr_reader :product_version

        def notify
          branch = product_version[Project::GitlabEe].ref
          logger.info('Notifying the coordinated pipeline creation')

          return if SharedStatus.dry_run?

          ReleaseTools::Slack::CoordinatedPipelineTagNotification
            .new(deploy_version: deploy_version, auto_deploy_branch: branch, tag_version: product_version.version)
            .execute
        end

        def sentry_release
          return if SharedStatus.dry_run?

          ReleaseTools::Deployments::SentryTracker.new.release(commit)
        end

        def store_deploy_version
          return if SharedStatus.dry_run?

          logger.info('Storing deploy version', deploy_version: deploy_version)

          File.write('deploy_vars.env', "DEPLOY_VERSION=#{deploy_version}")
        end

        def commit
          @commit ||= product_version[Project::GitlabEe].sha
        end

        def deploy_version
          omnibus_ref = product_version[Project::OmnibusGitlab].ref

          ReleaseTools::AutoDeploy::Version.new(omnibus_ref).to_package
        end
      end
    end
  end
end
