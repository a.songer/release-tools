# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      # CheckProduction verifies production status.
      #
      # It can authorize a deployment if DEPLOY_VERSION variable is set.
      class CheckProduction
        include ::SemanticLogger::Loggable

        def execute
          unless deploy_version.present?
            raise 'This task requires the DEPLOY_VERSION variable containing the package version to authorize'
          end

          manager = ReleaseTools::Promotion::Manager.new

          logger.info("Checking production promotion", deploy_version: deploy_version)
          manager.authorize!(omnibus_package_version, monthly_issue)
        end

        def monthly_issue
          monthly_version = ReleaseManagers::Schedule.new.active_version

          ReleaseTools::MonthlyIssue.new(version: monthly_version)
        end

        def omnibus_package_version
          ReleaseTools::Version.new(deploy_version)
        end

        def deploy_version
          ENV.fetch('DEPLOY_VERSION') do
            ReleaseTools::AutoDeploy::Tag.new.omnibus_package if ReleaseTools::AutoDeploy.coordinator_pipeline?
          end
        end
      end
    end
  end
end
