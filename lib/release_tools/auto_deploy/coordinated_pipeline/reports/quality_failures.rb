# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Reports
        class QualityFailures < ReleaseTools::Issue
          include ::SemanticLogger::Loggable

          NoFailuresFoundError = Class.new(StandardError)

          def initialize(environment:, coordinated_pipeline_id:)
            @environment = environment
            @coordinated_pipeline_id = coordinated_pipeline_id

            @pipelines = Quality::Fetcher
              .new(environment: environment, pipeline_id: coordinated_pipeline_id)
              .execute
          end

          def create
            if @pipelines.empty?
              logger.info(
                <<~MSG
                  No failing job was found.
                  It is possible that failing job was restarted before this job attempted to generate the report.
                  Please open an issue on https://gitlab.com/gitlab-org/release/tasks/-/issues and create the report manually.
                MSG
              )

              raise NoFailuresFoundError
            end

            create_issue
          end

          def title
            if multiple?
              "QA failures on #{environment}"
            else
              "QA failure: #{spec_failure}"
            end
          end

          def description
            variables = binding
            variables.local_variable_set(:environment, environment)
            variables.local_variable_set(:coordinated_pipeline_link, coordinated_pipeline_link)
            variables.local_variable_set(:auto_deploy_package, auto_deploy_package)
            variables.local_variable_set(:pipelines, pipelines)

            ERB
              .new(template, trim_mode: '-')
              .result(variables)
          end

          def assignees
            ReleaseManagers::Schedule.new.active_release_managers.collect(&:id)
          rescue ReleaseManagers::Schedule::VersionNotFoundError
            nil
          end

          def labels
            'release-blocker'
          end

          private

          attr_reader :environment, :pipelines, :coordinated_pipeline_id

          def create_issue
            return if SharedStatus.dry_run?

            response =
              Retriable.with_context(:api) do
                GitlabClient.create_issue(self, ReleaseTools::Project::Release::Tasks)
              end

            logger.info('Created issue to track QA failure', issue_url: response.web_url)

            response
          end

          def template
            template_path = File
              .expand_path('../../../../../templates/quality_failures_report.md.erb', __dir__)

            File.read(template_path)
          end

          def multiple?
            pipelines.count > 1
          end

          def spec_failure
            pipelines
              .first
              .jobs
              .first
              .title
          end

          def coordinated_pipeline_link
            Retriable.with_context(:api) do
              ReleaseTools::GitlabOpsClient
                .pipeline(ReleaseTools::Project::ReleaseTools, coordinated_pipeline_id)
                .web_url
            end
          end

          def auto_deploy_package
            ENV.fetch('DEPLOY_VERSION', nil)
          end
        end
      end
    end
  end
end
