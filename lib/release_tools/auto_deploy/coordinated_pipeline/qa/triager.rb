# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    module CoordinatedPipeline
      module Qa
        class Triager
          include ::SemanticLogger::Loggable

          def initialize(deploy_version:, environment:)
            @client = ReleaseTools::GitlabClient
            @deploy_version = deploy_version
            @environment = environment
          end

          def execute
            return unless Feature.enabled?(:quality_triager)

            logger.info('Searching quality issue', deploy_version: deploy_version, environment: environment)

            if qa_issue.nil?
              logger.info('No QA issue was found. Skipping')

              return
            end

            logger.info('QA issue found', web_url: qa_issue.web_url)

            return if SharedStatus.dry_run?

            notify_issue
            update_issue
            close_issue
          end

          private

          attr_reader :deploy_version, :environment, :client

          def qa_issue
            @qa_issue ||= Retriable.with_context(:api) do
              client.issues(project, options).first
            end
          end

          def project
            ReleaseTools::Project::Release::Tasks
          end

          def options
            {
              state: 'opened',
              author_id: ReleaseTools::Bot::GITLAB_COM_ID,
              search: "Environment: #{environment} Auto-deploy package: #{deploy_version}"
            }
          end

          def notify_issue
            logger.info('Adding comment to QA blocker issue', web_url: qa_issue.web_url)

            Retriable.with_context(:api) do
              client.create_issue_note(
                project,
                issue: qa_issue,
                body: "Pipeline succeeded on retry"
              )
            end
          end

          def update_issue
            logger.info('Updating issue with labels', web_url: qa_issue.web_url)

            labels =
              [
                'RootCause::FlakyTest',
                'Deploys-blocked-gstg::0.5hr',
                'Deploys-blocked-gprd::0.5hr'
              ]

            Retriable.with_context(:api) do
              client.edit_issue(
                project.to_s,
                qa_issue.iid,
                { labels: qa_issue.labels.concat(labels).join(', ') }
              )
            end
          end

          def close_issue
            logger.info('Closing issue', web_url: qa_issue.web_url)

            Retriable.with_context(:api) do
              client.close_issue(project, qa_issue)
            end
          end
        end
      end
    end
  end
end
