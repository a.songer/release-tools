# frozen_string_literal: true

module ReleaseTools
  module AutoDeploy
    # Convenience methods for validating and working with auto-deploy versions,
    # for both tags and packages.
    class Version < String
      VERSION_PATTERN = %r{
        \A
        (?<major>\d+)
        \.
        (?<minor>\d+)
        \.
        (?<timestamp>\d+)
        (?<separator>-|\+)
        (?<rails_sha>\h{11,})
        \.
        (?<omnibus_sha>\h{11,})
        \z
      }x

      def self.match?(version_string)
        version_string.match?(VERSION_PATTERN)
      end

      def initialize(str)
        unless str.match?(VERSION_PATTERN)
          raise ArgumentError, "invalid auto-deploy version string `#{str}`"
        end

        super
      end

      def package?
        captures.fetch('separator') == '-'
      end

      def tag?
        captures.fetch('separator') == '+'
      end

      def timestamp
        captures.fetch('timestamp')
      end

      def rails_sha
        captures.fetch('rails_sha')
      end

      def omnibus_sha
        captures.fetch('omnibus_sha')
      end

      def to_package
        sub('+', '-')
      end

      def to_tag
        sub('-', '+')
      end

      private

      def captures
        @captures ||= match(VERSION_PATTERN).named_captures
      end
    end
  end
end
