# frozen_string_literal: true

module ReleaseTools
  module Project
    class ChatOps < BaseProject
      REMOTES = {
        canonical: 'git@gitlab.com:gitlab-com/chatops.git',
        ops: 'git@ops.gitlab.net:gitlab-com/chatops.git',
        security: 'git@ops.gitlab.net:gitlab-com/chatops.git'
      }.freeze
    end
  end
end
