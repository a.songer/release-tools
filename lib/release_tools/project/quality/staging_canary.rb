# frozen_string_literal: true

module ReleaseTools
  module Project
    module Quality
      class StagingCanary < Base
        REMOTES = {
          canonical: 'git@gitlab.com:gitlab-org/quality/staging-canary.git',
          ops: 'git@ops.gitlab.net:gitlab-org/quality/staging-canary.git'
        }.freeze

        def self.environment
          'gstg-cny'
        end
      end
    end
  end
end
