# frozen_string_literal: true

module ReleaseTools
  module Deployments
    class BlockerIssue
      def initialize(data)
        @data = data
      end

      def title
        "#{data.web_url}+"
      end

      def root_cause
        return unless root_cause_label

        "~\"#{root_cause_label}\""
      end

      def root_cause_label
        labels
          .detect { |label| label.include?('RootCause') }
      end

      def with_deploys_blocked_label?
        labels.any? { |label| label.include?('Deploys-blocked-') }
      end

      def hours_gstg_blocked
        hrs_blocked(environment: 'gstg')
      end

      def hours_gprd_blocked
        hrs_blocked(environment: 'gprd')
      end

      def created_at
        Time.parse(data.created_at)
      end

      def ended_at
        duration = [hours_gprd_blocked, hours_gstg_blocked].max
        created_at + duration.hours
      end

      def need_to_annotate?
        !flaky_test? && !less_or_equal_30m?
      end

      attr_reader :data

      private

      def hrs_blocked(environment:)
        labels
          .grep(/Deploys-blocked-#{environment}/)
          .first&.gsub(/[^0-9.]/, '')
          .to_f
      end

      def labels
        data.labels
      end

      def less_or_equal_30m?
        (ended_at - created_at).to_i <= 30.minute.to_i
      end

      def flaky_test?
        root_cause_label&.include?('Flaky-Test')
      end
    end
  end
end
