# frozen_string_literal: true

module ReleaseTools
  module ReleaseEnvironment
    class QaNotifier
      include ::SemanticLogger::Loggable
      include ReleaseTools::AutoDeploy::Pipeline

      def initialize(pipeline_id:, environment_name:, release_environment_version:)
        @pipeline_id = pipeline_id
        @environment_name = environment_name
        @release_environment_version = release_environment_version
      end

      def execute
        logger.debug('Executing release environment notifier',
                     pipeline_id: pipeline_id,
                     environment_name: environment_name,
                     release_environment_version: release_environment_version)

        qa_job = find_qa_job

        if qa_job.present?
          logger.info('QA job found', pipeline_url: qa_job.web_url)

          send_slack_notification(qa_job)
        else
          logger.fatal('QA job not found')
        end
      end

      private

      attr_reader :pipeline_id, :environment_name, :release_environment_version

      def find_qa_job
        logger.info('Fetching QA job from pipeline', pipeline_id: pipeline_id)

        Retriable.with_context(:pipeline_created) do
          ReleaseTools::GitlabClient.pipeline_job_by_name(
            ReleaseTools::Project::GitlabEe,
            pipeline_id,
            "release-environments-qa"
          )
        end
      end

      def send_slack_notification(qa_job)
        ReleaseTools::Slack::ReleaseEnvironment::QaNotification.new(
          release_environment_version: release_environment_version,
          environment_name: environment_name,
          job: qa_job
        ).execute
      end
    end
  end
end
