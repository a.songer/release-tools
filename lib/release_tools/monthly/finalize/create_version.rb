# frozen_string_literal: true

module ReleaseTools
  module Monthly
    module Finalize
      class CreateVersion
        include ::SemanticLogger::Loggable

        def initialize
          @client = ReleaseTools::VersionClient
        end

        def execute
          logger.info('Creating release version', version: active_version)

          return if SharedStatus.dry_run?

          if version_exists?
            logger.info('Version exist, skipping')
            return
          end

          Retriable.with_context(:api) do
            client.create_version(active_version)
          end

          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(error_message, error: ex)

          send_slack_notification(:failed)

          raise
        end

        private

        attr_reader :client

        def active_version
          @active_version ||= GitlabReleases.active_version
        end

        def version_exists?
          response = client.get_version(active_version)
          response.respond_to?(:version)
        end

        def error_message
          <<~MSG
            The version failed to be created, retry this job. If the failure persists, proceed to create the version with https://version.gitlab.com/versions/new?version=#{active_version}
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Create new version',
            status: status,
            release_type: :monthly
          ).send_notification
        end
      end
    end
  end
end
