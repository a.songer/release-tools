# frozen_string_literal: true

module ReleaseTools
  module Security
    class TagPatchRelease
      include ::SemanticLogger::Loggable

      def initialize(version)
        @version = version
      end

      def execute
        $stdout
          .puts "Using security repository only!\n"
          .colorize(:red)

        Rake::Task['release:tag'].invoke(@version)
        send_slack_notification(:success)
      rescue StandardError => ex
        logger.fatal(error_message(ex), error: ex)
        send_slack_notification(:failed)
        raise
      end

      private

      def send_slack_notification(status)
        ReleaseTools::Slack::ReleaseJobEndNotifier.new(
          job_type: "Tag patch release version #{@version}",
          status: status,
          release_type: :patch
        ).send_notification
      end

      def error_message(exception)
        <<~MSG
          Tagging the patch release failed with error: #{exception.message}.
          Review the error log and consider retrying this job.
        MSG
      end
    end
  end
end
