# frozen_string_literal: true

require 'date'

module ReleaseTools
  module Security
    class TrackingIssue < ReleaseTools::Issue
      include ::SemanticLogger::Loggable

      # Represent the cadence to schedule patch releases
      RELEASE_CADENCE = 2.weeks

      def title
        prefix =
          if SharedStatus.critical_security_release?
            'Critical patch'
          else
            'Patch'
          end

        "#{prefix} release: #{versions_title}"
      end

      def confidential?
        true
      end

      def labels
        'upcoming security release,security,meta'
      end

      def project
        ReleaseTools::Project::GitlabEe
      end

      def versions_title
        next_security_release_versions.map! do |version|
          version.sub(/\d+$/, 'x')
        end.join(', ')
      end

      def due_date
        gitlab_releases_client.next_patch_release_date
      end

      def assignees
        release_managers.collect(&:id)
      rescue ReleaseManagers::Schedule::VersionNotFoundError
        nil
      end

      protected

      def template_path
        File.expand_path('../../../templates/security_release_tracking_issue.md.erb', __dir__)
      end

      def next_security_release_versions
        logger.info("Fetching the active versions for #{RELEASE_CADENCE.from_now}")

        minor_for_date = gitlab_releases_client
          .current_minor_for_date(RELEASE_CADENCE.from_now)

        gitlab_releases_client
          .previous_minors(minor_for_date)
          .map { |version| "#{version}.x" }
      end

      def release_managers
        active_version_for_date = gitlab_releases_client
          .version_for_date(RELEASE_CADENCE.from_now)

        version = ReleaseTools::Version.new(active_version_for_date)

        release_managers_schedule.authorized_release_managers(version)
      end

      def gitlab_releases_client
        ReleaseTools::GitlabReleasesClient
      end

      def release_managers_schedule
        @release_managers_schedule ||= ReleaseManagers::Schedule.new
      end
    end
  end
end
