# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class NotifyNextReleaseManagers
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotNotifyError = Class.new(StandardError)

        def execute
          if security_tracking_issue.present?
            logger.info(
              'Notify next release managers about the next patch release',
              issue: security_tracking_issue.iid
            )

            return if SharedStatus.dry_run?

            post_issue_note
            send_slack_notification(:success)
          else
            logger.fatal('Security tracking issue not found')

            raise CouldNotNotifyError
          end
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotNotifyError
        end

        private

        def post_issue_note
          Retriable.with_context(:api) do
            client.create_issue_note(
              project,
              issue: security_tracking_issue,
              body: notification_message
            )
          end
        end

        def project
          @project ||= ReleaseTools::Project::GitlabEe
        end

        def client
          @client ||= ReleaseTools::GitlabClient
        end

        def next_release_managers
          logger.info('Fetching assignees from the security tracking issue', issue: security_tracking_issue.web_url)

          security_tracking_issue.assignees.map(&:username).map do |username|
            "@#{username}"
          end
        end

        def notification_message
          <<~MSG.strip
            Hello #{next_release_managers.join(', ')}. The next patch release has been automatically scheduled
            for #{security_tracking_issue.due_date}. Please review the date and make sure is feasible.

            If the date has been changed, click on [this pipeline](https://ops.gitlab.net/gitlab-org/release/tools/-/pipelines/new?var[PATCH_RELEASE_METRIC]=open)
            **after updating the date on this issue** to update the patch release dashboard.
          MSG
        end

        def failure_message
          <<~MSG
            Notifying next release managers failed. Ensure gitlab-release-tools-bot has access to the gitlab-org/gitlab repository and retry this job.
            If the job continues to fail, notify #{next_release_managers.join(', ')} about the patch release on #{security_tracking_issue.web_url}
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Notify Next Release Managers',
            status: status,
            release_type: :patch
          ).send_notification
        end
      end
    end
  end
end
