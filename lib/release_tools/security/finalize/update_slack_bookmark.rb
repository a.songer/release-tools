# frozen_string_literal: true

module ReleaseTools
  module Security
    module Finalize
      class UpdateSlackBookmark
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        BOOKMARK_TITLE = 'Next Patch Release'
        CouldNotNotifyError = Class.new(StandardError)

        def execute
          logger.info(
            'Updating slack bookmark in releases channel',
            issue: security_tracking_issue&.iid
          )

          if security_tracking_issue.present?
            return if SharedStatus.dry_run?

            update_bookmark
            send_slack_notification(:success)
          else
            logger.fatal('Security tracking issue not found')

            raise CouldNotNotifyError
          end
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotNotifyError
        end

        private

        def update_bookmark
          unless patch_bookmark
            logger.fatal('Bookmark could not be found')

            raise CouldNotNotifyError
          end

          ReleaseTools::Slack::Bookmark.edit(
            channel: channel,
            bookmark: patch_bookmark['id'],
            link: security_tracking_issue.web_url
          )
        end

        def patch_bookmark
          bookmarks.find do |bookmark|
            bookmark['title'] == BOOKMARK_TITLE
          end
        end

        def bookmarks
          @bookmarks ||= ReleaseTools::Slack::Bookmark.list(channel: channel)['bookmarks']
        end

        def channel
          ReleaseTools::Slack::RELEASES
        end

        def failure_message
          <<~MSG
            The bookmark for the #releases channel could not be updated.
            If the job continues to fail, update or create a bookmark for "Next Patch Release"
            with the link: #{security_tracking_issue&.web_url}
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Update slack bookmark',
            status: status,
            release_type: :patch
          ).send_notification
        end
      end
    end
  end
end
