# frozen_string_literal: true

module ReleaseTools
  module Security
    module Prepare
      class NotifyJihu
        include ::SemanticLogger::Loggable
        include ::ReleaseTools::Security::IssueHelper

        CouldNotNotifyError = Class.new(StandardError)

        def initialize
          @versions = ReleaseTools::Versions.next_versions.join(', ')
          @project = ReleaseTools::Project::Jihu
          @client = ReleaseTools::GitlabClient
        end

        def execute
          logger.info(
            'Notify JiHu about the patch release',
            due_date: security_tracking_issue.due_date,
            versions: versions,
            issue: jihu_issue.iid
          )

          return if SharedStatus.dry_run?

          post_issue_note
          send_slack_notification(:success)
        rescue StandardError => ex
          logger.fatal(failure_message, error: ex)

          send_slack_notification(:failed)

          raise CouldNotNotifyError
        end

        private

        attr_reader :versions, :project, :client

        def jihu_issue
          @jihu_issue ||= Retriable.with_context(:api) do
            client.issues(
              project,
              labels: 'engineering',
              state: 'opened',
              search: 'GitLab patch and security release dates'
            ).first
          end
        end

        def post_issue_note
          Retriable.with_context(:api) do
            client.create_issue_note(
              project,
              issue: jihu_issue,
              body: notification_message
            )
          end
        end

        def notification_message
          <<~MSG.strip
            @chaomao We've started the preparation for a patch release scheduled for #{reference_issue.due_date}.

            The following packages will be released: #{versions}.
          MSG
        end

        def failure_message
          <<~MSG
            Notifying JiHu failed. Ensure gitlab-release-tools-bot has access to the JiHu repository and retry this job.
            If the job continues to fail, notify JiHu about the patch release on #{jihu_issue.web_url}
          MSG
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Notify JiHu',
            status: status,
            release_type: :patch
          ).send_notification
        end
      end
    end
  end
end
