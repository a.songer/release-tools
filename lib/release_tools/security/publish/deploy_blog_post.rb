# frozen_string_literal: true

module ReleaseTools
  module Security
    module Publish
      class DeployBlogPost
        include ReleaseTools::Security::MergeRequestHelper
        include ::SemanticLogger::Loggable

        def initialize
          @client = ReleaseTools::GitlabClient
          @blog_post = canonical_blog_merge_request
        end

        def execute
          return if SharedStatus.dry_run?

          approve_merge_request
          add_merge_request_to_merge_train
          send_slack_notification(:success)
        rescue Gitlab::Error::Error => ex
          logger.fatal(error_message, error: ex)
          send_slack_notification(:failed)

          raise
        end

        private

        attr_reader :client, :blog_post

        def handbook_project
          ReleaseTools::Project::WWWGitlabCom
        end

        # The `ApprovalService` is not compatible with the handbook merge requests
        # because they always return `approved = true` regardless if they're approved or not,
        # this is due to how the approval rules are configured in the handbook repository.
        # Because of it, this method relies on the information of the `approved_by` attribute and
        # verifies if at least one person has approved the blog post, if that is the case,
        # the approval is skipped.
        def approve_merge_request
          logger.info('Approving the blog post', blog_post: blog_post.web_url)

          if approval_information.approved_by.empty?
            logger.info('Blog post has no approvals, proceeding to approve')

            Retriable.with_context(:api) do
              client
                .approve_merge_request(handbook_project, blog_post.iid)
            end
          else
            logger.info('Blog post has one approval, skipping approval')
          end
        end

        def approval_information
          logger.info('Fetching blog post approval information')

          Retriable.with_context(:api) do
            client.merge_request_approvals(handbook_project, blog_post.iid)
          end
        end

        def add_merge_request_to_merge_train
          logger.info('Adding blog post to the merge train', blog_post: blog_post.web_url)

          Retriable.with_context(:api) do
            client.add_to_merge_train(
              blog_post,
              blog_post.sha,
              graphql: false,
              pipeline_succeed: pending_pipeline?
            )
          end
        end

        def pending_pipeline?
          pipeline = Retriable.with_context(:api) do
            client.merge_request_pipelines(handbook_project, blog_post.iid).first
          end

          %w(running created).include?(pipeline.status)
        end

        def send_slack_notification(status)
          ReleaseTools::Slack::ReleaseJobEndNotifier.new(
            job_type: 'Deploy blog post',
            status: status,
            release_type: :patch
          ).send_notification
        end

        def error_message
          <<~MSG
            Something went wrong when approving and setting the blog post to auto-merge, consider triggering
            a new pipeline and retry this job. If the failure persists, manually approve and set the blog post
            to auto-merge: #{blog_post.web_url}.
          MSG
        end
      end
    end
  end
end
