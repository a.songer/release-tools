# frozen_string_literal: true

module ReleaseTools
  module Slack
    class ReleasePipelineStartNotifier
      include ::SemanticLogger::Loggable
      include ReleaseTools::Slack::Utilities
      include ::ReleaseTools::Slack::ReleasePipelineHelpers

      # stage - the stage of the security pipeline, start and finalize are supported
      # release_type - :patch and :monthly are supported
      def initialize(stage:, release_type:)
        @stage = stage
        @release_type = release_type
      end

      def execute
        logger.info("Notifying the stage of a #{release_type} pipeline", stage: stage, release_manager: user_name, pipeline_url: pipeline_url)

        ReleaseTools::Slack::Message.post(
          channel: ReleaseTools::Slack::F_UPCOMING_RELEASE,
          message: slack_message,
          blocks: slack_blocks
        )
      end

      private

      attr_reader :stage, :release_type

      def slack_message
        "Release manager #{user_name} #{fallback_message[stage]}"
      end

      def fallback_message
        {
          backport_merge: backport_merge_message,
          early_merge: early_merge_message,
          finalize: finalize_message,
          publish: publish_message,
          release_preparation: release_preparation_message,
          start: start_message,
          tag: tag_message
        }
      end

      def backport_merge_message
        "has started the backport merging steps for stable branches security MRs, these jobs will be executed as part of #{pipeline_url}."
      end

      def early_merge_message
        "has started the early merging steps for default branch security MRs, these jobs will be executed as part of #{pipeline_url}."
      end

      def finalize_message
        "has started the final steps of a #{release_type} release, these steps will be executed as part of #{pipeline_url}."
      end

      def publish_message
        "has started the publishing steps of a #{release_type} release, these jobs will be executed as part of #{pipeline_url}."
      end

      def release_preparation_message
        "has started the release preparation steps, these jobs will be executed as part of #{pipeline_url}."
      end

      def start_message
        "has started a #{release_type} release, initial steps will now be run in #{pipeline_url}."
      end

      def tag_message
        "has started the tagging steps for the patch release versions, these jobs will be executed as part of #{pipeline_url}."
      end

      def slack_blocks
        [
          {
            type: 'section',
            text: ReleaseTools::Slack::Webhook.mrkdwn(section_block)
          },
          {
            type: 'context',
            elements: [clock_context_element]
          }
        ]
      end

      def section_block
        [].tap do |text|
          text << EMOJI[release_type]
          text << slack_message
        end.join(' ')
      end
    end
  end
end
