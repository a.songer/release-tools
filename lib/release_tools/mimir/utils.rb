# frozen_string_literal: true

module ReleaseTools
  module Mimir
    module Utils
      module_function

      # rubocop:disable Metrics/ParameterLists
      def explore_pane_url(query:, from:, to:, datasource: Mimir::DATASOURCE_ALL_ENVS, range: true, instant: true)
        pane_str = pane_json({ query:, from:, to:, datasource:, range:, instant: })

        "#{Mimir::GRAFANA_URL}&panes=#{CGI.escape(pane_str)}"
      end
      # rubocop:enable Metrics/ParameterLists

      private

      def pane_json(opts)
        {
          abc: {
            datasource: opts[:datasource],
            queries:
              [
                {
                  refId: "A",
                  expr: opts[:query],
                  range: opts[:range],
                  instant: opts[:instant],
                  datasource:
                    {
                      type: "prometheus",
                      uid: opts[:datasource]
                    },
                  editorMode: "code"
                }
              ],
            range:
              {
                from: opts[:from],
                to: opts[:to]
              }
          }
        }.to_json
      end
      module_function :pane_json
    end
  end
end
