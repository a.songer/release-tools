# frozen_string_literal: true

module ReleaseTools
  module Metrics
    class ReleaseMetricUpdater
      include ::SemanticLogger::Loggable

      MONTHLY_STATUSES = %i[open announced tagged_rc].freeze
      PATCH_STATUSES = %i[open warning closed].freeze

      def initialize
        @prometheus = ReleaseTools::Prometheus::Query.new
      end

      def execute
        return unless Feature.enabled?(:release_status_metric_update)

        update_monthly_release_status
        update_patch_release_status
      end

      private

      def releases_client
        @releases_client ||= ReleaseTools::GitlabReleasesClient
      end

      def monthly_active_version
        ReleaseTools::Version.new(releases_client.version_for_date(Date.today)).to_minor
      end

      def next_patch_release_date
        @next_patch_release_date ||= releases_client.next_patch_release_date
      end

      def patch_next_versions
        # Finds the current minor version when the next patch release is scheduled
        # Then finds the previous minor versions for backports

        minor_version_for_patch_release = releases_client.current_minor_for_date(next_patch_release_date)

        releases_client
          .previous_minors(minor_version_for_patch_release)
          .join(" ")
      end

      def monthly_release_status(version)
        @prometheus.monthly_release_status(version)
      end

      def patch_release_status(versions)
        @prometheus.patch_release_status(versions)
      end

      def update_monthly_release_status
        monthly_status = monthly_release_status(monthly_active_version)

        # Status on the MonthlyReleaseStatus metric
        # 1 = open, 2 = announced, 3 = tagged_rc
        # so we need to subtract 1 to match it to MONTHLY_STATUSES
        status = MONTHLY_STATUSES[monthly_status - 1]
        ReleaseTools::Metrics::MonthlyReleaseStatus
          .new(status: status)
          .execute
      end

      def update_patch_release_status
        patch_status = patch_release_status(patch_next_versions)

        # Status on the PatchReleaseStatus metric
        # 1 = open, 2 = warning, 3 = closed
        # so we need to subtract 1 to match it to PATCH_STATUSES
        status = PATCH_STATUSES[patch_status - 1]
        ReleaseTools::Metrics::PatchReleaseStatus
          .new(status: status)
          .execute
      end
    end
  end
end
