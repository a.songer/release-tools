# frozen_string_literal: true

module ReleaseTools
  module Metrics
    # Publish metrics related to monthly release information
    # Contains version and release date as labels
    class MonthlyReleaseStatus
      include ::SemanticLogger::Loggable

      METRIC = :release_monthly_status

      # Open (1): Any commit that reached production is expected to be released with the next monthly release
      # Announced (2): Signal the Final RC tagging date is getting closer
      # Tagged (3): Final RC has been tagged
      STATUSES = {
        open: 1,
        announced: 2,
        tagged_rc: 3
      }.freeze

      def initialize(status:)
        @status = status
        @client = ReleaseTools::Metrics::Client.new
      end

      def execute
        return unless Feature.enabled?(:release_status_metric)

        logger.info('Setting release_monthly_status metric', status: status_value, labels: "#{release_date},#{version}")
        return if ReleaseTools::SharedStatus.dry_run?
        return if STATUSES[status].nil?

        client.set(METRIC, status_value, labels: "#{release_date},#{version}")
      end

      private

      attr_reader :status, :client

      def status_value
        STATUSES[status]
      end

      def version_for_date(date)
        ReleaseTools::Version.new(ReleaseTools::GitlabReleasesClient.version_for_date(date)).to_minor
      end

      def version
        #  "open" status is set at the end of a minor release for the upcoming monthly release
        if @status == :open
          version_for_date(Date.tomorrow)
        else
          version_for_date(Date.today)
        end
      end

      def release_date
        Date.parse(
          ReleaseTools::GitlabReleasesClient.upcoming_releases[version]
        )
      end
    end
  end
end
