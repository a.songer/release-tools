# frozen_string_literal: true

module ReleaseTools
  module Metrics
    # Publish metrics related to patch release information
    # Contains version and release date as labels
    class PatchReleaseStatus
      include ::SemanticLogger::Loggable
      include ReleaseTools::Security::IssueHelper

      METRIC = :release_patch_status

      # Open (1): Bug fixes and MRs associated with security issues labelled `security-target` are expected to be included in the next patch release.
      # Warning (2): Signals that teams should get bug and security fixes ready to merge.
      # Closed (3): Default branch MRs have been merged, no further bug or security fixes will be included.
      STATUSES = {
        open: 1,
        warning: 2,
        closed: 3
      }.freeze

      def initialize(status:)
        @status = status
        @client = ReleaseTools::Metrics::Client.new
      end

      def execute
        return unless Feature.enabled?(:release_status_metric)

        logger.info(
          'Setting release_patch_status metric',
          status: status_value,
          labels: "#{release_date},#{versions}"
        )

        return if ReleaseTools::SharedStatus.dry_run?
        return if status_value.nil?

        # There's a scheduled pipeline "metrics:patch_release:warning" that runs every week to create a warning status metric.
        # It should only create the metric if the release date is next week
        if status == :warning && !release_date_next_week?
          logger.info(
            'Not creating warning metric; patch release date is not next week',
            release_date: release_date
          )

          return
        end

        client.set(METRIC, status_value, labels: "#{release_date},#{versions}")
      end

      private

      attr_reader :status, :client

      def status_value
        STATUSES[status]
      end

      def releases_client
        @releases_client ||= ReleaseTools::GitlabReleasesClient
      end

      def release_date_next_week?
        now = Time.now.utc

        release_date >= now.next_week(:monday) && release_date <= now.next_week(:friday)
      end

      def next_patch_release_date
        @next_patch_release_date ||= releases_client.next_patch_release_date
      end

      # Returns space-separated minor versions for the next patch release
      # e.g. "16.10 16.9 16.8"
      def versions
        # Finds the current minor version when the next patch release is scheduled
        # Then finds the previous minor versions for backports
        minor_version_for_patch_release = releases_client
          .current_minor_for_date(next_patch_release_date)

        logger.info(
          'Minor version for next patch release date',
          minor_version_for_patch_release: minor_version_for_patch_release,
          next_patch_release_date: next_patch_release_date
        )

        releases_client
          .previous_minors(minor_version_for_patch_release)
          .join(" ")
      end

      def release_date
        date = if security_tracking_issue.present?
                 security_tracking_issue.due_date
               else
                 next_patch_release_date
               end

        Date.parse(date)
      end
    end
  end
end
