<!--

Please do not add any additional steps to this template. Instead, add a job to the
security_release pipeline. Development guidelines can be found in doc/release-pipelines.md.

If you make any changes to this template in Release Tools, also make sure to
update any existing release issues (if necessary). If you've edited an active release issue
please consider making the same change in the template.

-->

## First steps

- [ ] Start the `security_release_prepare:start` job in the security pipeline: <%= security_pipeline.web_url %>
   - [ ] Ensure the `security_release:prepare` stage completes before continuing to the next section.
- [ ] Modify the dates below to accurately reflect the plan of action. For example, if the planned due date is 28th, update the section titled "One day before due date" to "On 27th (One day before due date)".
<% if critical? -%>
- [ ] Post a message to `#g_dedicated-team` channel in Slack to inform about this critical patch release. Dedicated will
need to make a plan to patch their installations.
- [ ] Check if the security fix is associated with any of the [projects under GitLab managed versioning](https://gitlab.com/gitlab-org/release/docs/-/tree/master/components/managed-versioning#components-under-managed-versioning)
  (<%= unsupported_projects_list %>).
   - [ ] If this is the case, [follow the release manager instructions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process) and adjust this issue to include any additional steps needed.
<% end %>

<% if regular? -%>
## Two days before due date

- [ ] Check if the [security tracking issue](<%= security_release_tracking_issue&.web_url %>) contains any linked issues for [projects under GitLab managed versioning](https://gitlab.com/gitlab-org/release/docs/-/tree/master/components/managed-versioning#components-under-managed-versioning) that are not automatically processed (<%= unsupported_projects_list %>).
   - [ ] If there are any linked issues, [follow the release manager instructions](https://gitlab.com/gitlab-org/release/docs/-/blob/master/components/managed-versioning/security_release.md#release-manager-process) and adjust this issue to include any additional steps needed.
   - [ ] If a Gitaly security fix is included in the upcoming patch release, follow the [How to deal with Gitaly security fixes](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/how_to_handle_gitaly_security_merge_requests.md) guide.
- [ ] Before running the default merge chatops command, disable the [`security-target issue processor` pipeline schedule](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules)
  to ensure no other issues are linked to the security tracking issue and no linked issues are inadvertently unlinked after this point.
- [ ] Check the issues linked to the security tracking issue. If there are any that DO NOT have the ~"security-target"
  label applied, check to make sure they are expected to be included, otherwise unlink them and point the assignees to the
  [correct process](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md).
<% if patch_release_pipeline? %>
- [ ] Start the `security_release_early_merge:start` job of the security pipeline: <%= security_pipeline.web_url %>
<% else %>
- [ ] Merge the merge requests targeting default branches
  ```
  # In Slack
  /chatops run release merge --security --default-branch
  ```
<% end %>
- [ ] Verify that the table of issues in the security tracking issue has been updated showing the default MRs have
  been merged or set to Merge When Pipeline Succeeds (MWPS/auto-merge).
<% end %>

## One day before the due date

If this date is on a weekend, do this work on the next working day.

- [ ] Start the `security_release_release_preparation:start` job of the security pipeline: <%= security_pipeline.web_url %>

<% if regular? -%>
- [ ] Check that all MRs merged into the default branch have been deployed to production:
   ```sh
   # In Slack:
   /chatops run auto_deploy security_status
   ```

   NOTE: This only checks `gitlab-org/security/gitlab`. If other projects have security MRs you should verify those manually.
- [ ] Make sure to [execute the post-deploy migration pipeline] to ensure that all post-deploy migrations have been executed:
  `/chatops run post_deploy_migrations execute`
- [ ] Merge backports and any other merge request pending:
   ```sh
   # In Slack:
   /chatops run release merge --security
   ```
- [ ] If any merge requests could not be merged, investigate what needs to be done to resolve the issues. Do **not** proceed unless it has been determined safe to do so.
<% else -%>
- [ ] Merge critical security merge requests using the UI.
- [ ] Follow the process for [speed the auto-deploy process for security merge requests]
- [ ] Deploy all the fixes to production.
- [ ] Merge all backports into the appropriate stable branches (if required). Stable branches will diverge at this point. Any further fixes must target security branches.
<% end -%>
- [ ] Ensure tests are green in CE and green in EE
   ```sh
   # In Slack:
   /chatops run release status --security
   ```
- [ ] If all the security issues have been deployed to production, consider tagging.

## On the Due Date

### Packaging

- [ ] Ensure tests are green in CE and green in EE
   ```sh
   # In Slack:
   /chatops run release status --security
   ```
**For the next task: Waiting between pipelines is necessary as they may otherwise fail to
concurrently push changes to the same project/branch.**
<% if patch_release_pipeline? %>
- [ ] Start the `security_release_tag:start` job of the security pipeline to tag the patch release versions: <%= security_pipeline.web_url %>
<% else %>
<% versions.each do |version| -%>
- [ ] Tag the <%= version.to_patch %> patch release, and wait for the pipeline to finish: `/chatops run release tag --security <%= version.to_patch %>`
<% end %>
<% end %>
- [ ] Check that EE and CE packages are built. Please note the completion of the `RAT-Tag` job on the `slow_jobs` stage is not required for the next steps.
  <% versions.each do |version| -%>
  - <%= version.to_patch %>: [EE packages](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: true) %>) and [CE packages](https://dev.gitlab.org/gitlab/omnibus-gitlab/commits/<%= version.to_omnibus %>)
  <% end %>

- [ ] Check that the CNG Images are built. Do not play any manual jobs.
  <% versions.each do |version| -%>
  - <%= version.to_patch %>: [CNG builds](https://dev.gitlab.org/gitlab/charts/components/images/-/pipelines?ref=<%= version.tag(ee: true) %>)
  <% end %>

### Deploy

- [ ] Verify that release.gitlab.net is running the latest patch version
  - Check in Slack `#announcements` channel
  - Go to https://release.gitlab.net/help

## Release

**Consider communicating with the AppSec counterpart before publishing to sync on the time of releasing the blog post. Emails to the security mailing list are normally handled as a follow up task and should not delay release tasks**

<% versions.each do |version| -%>
- [ ] Publish <%= version.to_patch %> via ChatOps:
   ```
   /chatops run publish --security <%= version %>
   ```
<% end %>

- [ ] Verify with AppSec release managers if the blog post is ready to be published. **Do not proceed until AppSec has given green light**
- [ ] Start the `security_release_publish:start` stage of the security pipeline: <%= security_pipeline.web_url %>
- [ ] Verify that the `check-packages` job completes:
<% versions.each do |version| -%>
  - [ ] EE `check-packages` on [<%= version.to_omnibus(ee: true) %>](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: true) %>)
  - [ ] CE `check-packages` on [<%= version.to_omnibus(ee: false) %>](https://dev.gitlab.org/gitlab/omnibus-gitlab/-/commits/<%= version.to_omnibus(ee: false) %>)
<% end %>
- [ ] Verify that Docker images appear on `hub.docker.com`: [EE](https://hub.docker.com/r/gitlab/gitlab-ee/tags) / [CE](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
- [ ] Create the versions:
<% versions.each do |version| -%>
  - [ ] Create `<%= version %>` version on [version.gitlab.com](https://version.gitlab.com/versions/new?version=<%= version %>). **Be sure to mark it as a security release. From the `Security Release` dropdown choose `<%= version_type%>`**. After it is created, the `Vulnerability Type` column should indicate `No` for the new version.
<% end %>

### Final steps

- [ ] Start the `security_release_finalize:start` job in the security pipeline: <%= security_pipeline.web_url %>
- [ ] Sync the GitLab default branch by using the merge-train project:
  - [ ] Disable the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` [pipeline schedule on the merge-train].
  - [ ] Trigger the `gitlab-org/security/gitlab@master -> gitlab-org/gitlab@master` [pipeline schedule on the merge-train] and wait until it finishes. This pipeline will attempt to sync the GitLab default branch.
  - [ ] If the sync fails, repeat the above step.
- [ ] If after 5 times the sync by the merge train continues to fail, use the previous strategy to sync the GitLab project:
  - [ ] Disable the `merge_train_to_canonical` [feature flag on ops].
  - [ ] Enable the `gitlab-org/gitlab@master -> gitlab-org/security/gitlab@master` [pipeline schedule on the merge-train].
  - [ ] Execute the `sync_remotes` task on Slack: `/chatops run release sync_remotes --security`. In this case, if the sync fails, a merge request will be created and release manager intervention will be required.

- [ ] Verify all remotes are synced:

   ```sh
   # In Slack
   /chatops run mirror status
   ```

   If conflicts are found, [manual intervention will be needed to sync the repositories](https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/security/manually-sync-release-tag.md).

[execute the post-deploy migration pipeline]: https://gitlab.com/gitlab-org/release/docs/-/tree/master/general/post_deploy_migration#how-to-execute-post-deploy-migrations
[next set of release managers]: https://about.gitlab.com/community/release-managers/
[running a pipeline in OPS]: https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules
[pipeline schedule on the merge-train]: https://ops.gitlab.net/gitlab-org/merge-train/-/pipeline_schedules
[feature flag on ops]: https://ops.gitlab.net/gitlab-org/release/tools/-/feature_flags
[speed up the auto-deploy process for security merge requests]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/how_to_speed_auto_deploy_process_for_urgent_merge_requests.md#for-security-merge-requests
