#!/usr/bin/env bash

# This script will install the ASDF plugins required for this project
# Inspired by https://gitlab.com/gitlab-com/runbooks/-/blob/master/scripts/install-asdf-plugins.sh

set -euo pipefail
IFS=$'\n\t'

# Temporary transition over to mise from asdf
# see https://gitlab.com/gitlab-com/runbooks/-/issues/134
# for details
setup_asdf() {
  # shellcheck source=/dev/null
  source "$ASDF_DIR/asdf.sh"

  plugin_list=$(asdf plugin list || echo)

  install_plugin() {
    local plugin=$1

    if ! echo "${plugin_list}" | grep -q "${plugin}"; then
      echo "# Installing plugin" "$@"
      asdf plugin add "$@" || {
        echo "Failed to install plugin:" "$@"
              exit 1
            } >&2
    fi

    echo "# Installing ${plugin} version"
    asdf install "${plugin}" || {
      echo "Failed to install plugin version: ${plugin}"
          exit 1
        } >&2

    # Use this plugin for the rest of the install-asdf-plugins.sh script...
    asdf shell "${plugin}" "$(asdf current "${plugin}" | awk '{print $2}')"
  }
}

setup_mise() {
  mise_command=$1
  temp_MISE_SHORTHANDS_FILE=$(mktemp)
  trap 'do_mise_install' EXIT

  do_mise_install() {
    cat "$temp_MISE_SHORTHANDS_FILE"
    MISE_SHORTHANDS_FILE=$temp_MISE_SHORTHANDS_FILE $mise_command install -y
    rm -f "$temp_MISE_SHORTHANDS_FILE"
  }

  install_plugin() {
    local plugin=$1
    local source=${2-}

    # No source? rtx defaults should suffice.
    if [[ -z $source ]]; then return; fi

    # See https://mise.jdx.dev/configuration.html#mise-shorthands-file-config-mise-shorthands-toml
    echo "$plugin = \"$source\"" >>"$temp_MISE_SHORTHANDS_FILE"
  }
}

if command -v mise >/dev/null; then
  setup_mise $(which mise)
elif command -v rtx >/dev/null; then
  setup_mise $(which rtx)
elif [[ -n ${ASDF_DIR-} ]]; then
  setup_asdf
fi

# Install golang first as some of the other plugins require it.
install_plugin golang

install_plugin golangci-lint
install_plugin mockery
install_plugin ruby
install_plugin ringctl https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra.git
