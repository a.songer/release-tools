# frozen_string_literal: true

source "https://rubygems.org"

gem 'activesupport', '~> 7.1.0'
# chef-api is deprecated but brings in fewer dependencies
# than the official chef gem
gem 'chef-api', '~> 0.10.10'
gem 'colorize', '~> 1.1.0'
gem 'cvss-suite', '~> 3.2.0'
gem 'gitlab', '~> 4.19.0'
gem 'gitlab-releases', '~> 0.2.9'
gem 'graphql-client', '~> 0.22.0'
gem 'http', '~> 5.1.0'
gem 'parallel', '~> 1.25.0'
gem 'rake', '~> 13.2.0'
gem 'retriable', '~> 3.1.2'
gem 'rugged', '~> 1.7.0'
gem 'semantic_logger', '~> 4.15.0'
gem 'sentry-ruby', '~> 5.8'
gem 'slack-ruby-block-kit', '~> 0.24.0'
gem 'tzinfo-data', '~> 1.2024.0' # Required for CI environments where the OS might not have timezone data.
gem 'unleash', '~> 5.0.0'
gem 'version_sorter', '~> 2.3.0'

group :metrics do
  gem 'prometheus-client', '~> 2.1.0'
  gem 'state_machines', '~> 0.6.0'
end

group :tracing do
  gem 'opentelemetry-exporter-otlp', '~> 0.27.0'
  gem 'opentelemetry-sdk', '~> 1.4.0'
end

group :development, :test do
  gem 'byebug', '~> 11.1.0'
  gem 'climate_control', '~> 1.2.0'
  gem 'factory_bot', '~> 6.4.0'
  gem 'fuubar', require: false
  gem 'gitlab-dangerfiles', '~> 4.7.0', require: false
  gem 'lefthook', '~> 1.6.0', require: false
  gem 'pry', '~> 0.14.0'
  gem 'rspec', '~> 3.13.0'
  gem 'rspec_junit_formatter', '~> 0.6.0', require: false
  gem 'rspec-parameterized', '>= 1.0.0'
  gem 'rubocop', '~> 1.63.0'
  gem 'rubocop-performance', '~> 1.21.0'
  gem 'rubocop-rspec', '~> 2.31.0'
  gem 'simplecov', '~> 0.22.0'
  gem 'timecop', '~> 0.9.0'
  gem 'vcr', '~> 6.2.0'
  gem 'webmock', '~> 3.23.0'
end
