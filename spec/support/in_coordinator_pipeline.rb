# frozen_string_literal: true

module InCoordinatorPipeline
  # Set the `CI_COMMIT_TAG` environment variable to simulate a coordinator pipeline
  def in_coordinator_pipeline(tag: 'v1.2.3', &)
    ClimateControl.modify(CI_COMMIT_TAG: tag, &)
  end
end

RSpec.configure do |config|
  config.include InCoordinatorPipeline
end
