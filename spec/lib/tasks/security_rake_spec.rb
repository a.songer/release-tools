# frozen_string_literal: true

require 'rake_helper'

describe 'security tasks', :rake do
  after do
    # rake tasks in the security namespace run the force_security task altering
    # the global state
    ENV.delete('SECURITY')
  end

  describe 'sync_git_tags', task: 'security:sync_git_tags' do
    it 'syncs git tags' do
      expect(ReleaseTools::Security::SyncGitRemotesService).to receive(:new)
        .with(['1.0', '2.0', 'v3.1'])
        .and_return(instance_double(ReleaseTools::Security::SyncGitRemotesService, execute: true))

      task.invoke('1.0 2.0 v3.1')
    end
  end

  describe 'prepare:review_security_fixes', task: 'security:prepare:review_security_fixes' do
    it 'verifies the fixes included in the patch release' do
      expect(ReleaseTools::Security::Prepare::FixesVerifier).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Prepare::FixesVerifier, execute: true))

      task.invoke
    end
  end

  describe 'prepare:appsec_issue', task: 'security:prepare:appsec_issue' do
    it 'creates the AppSec task issue' do
      expect(ReleaseTools::Security::AppSecIssue).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::AppSecIssue, title: 'foo', description: 'baz', create: true))

      task.invoke
    end
  end

  describe 'prepare:comms_issue', task: 'security:prepare:comms_issue' do
    it 'creates the AppSec task issue' do
      expect(ReleaseTools::Security::CommsTaskIssue).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::CommsTaskIssue, title: 'foo', description: 'baz', create: true))

      task.invoke
    end
  end

  describe 'publish:move_blog_post', task: 'security:publish:move_blog_post' do
    it 'moves the blog post to the handbook canonical repo' do
      expect(ReleaseTools::Security::Publish::MoveBlogPost).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Publish::MoveBlogPost, execute: true))

      task.invoke
    end
  end

  describe 'publish:deploy_blog_post', task: 'security:publish:deploy_blog_post' do
    it 'publishes the patch release blog post' do
      expect(ReleaseTools::Security::Publish::DeployBlogPost).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Publish::DeployBlogPost, execute: true))

      task.invoke
    end
  end

  describe 'finalize:enable_security_target_processor', task: 'security:finalize:enable_security_target_processor' do
    it 'enables the security target processor' do
      expect(ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor, execute: true))

      task.invoke
    end
  end

  describe 'process_security_target_issues', task: 'security:process_security_target_issues' do
    it 'runs the processor' do
      expect(ReleaseTools::Security::TargetIssuesProcessor).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::TargetIssuesProcessor, execute: true))

      task.invoke
    end
  end

  describe 'finalize:create_release_status_metric', task: 'security:finalize:create_release_status_metric' do
    it 'creates a new patch release status metric with open status' do
      expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :open)
        .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

      task.invoke
    end
  end

  describe 'tag_jobs', task: 'security:tag_jobs' do
    let(:versions) { ['1.0.1', '1.1.1', '1.2.3'] }
    let(:expected_yaml) do
      {
        ".with-bundle" => {
          "before_script" => [
            "bundle install --jobs=$(nproc) --retry=3 --quiet"
          ]
        },
        "stages" => versions.map { |version| "security_release:tag:#{version}" },
        "security_release_tag:1.0.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:tag:1.0.1",
          "script" => [
            "bundle exec rake security:tag[1.0.1]"
          ],
          "extends" => ".with-bundle"
        },
        "security_release_tag:1.1.1" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:tag:1.1.1",
          "script" => [
            "bundle exec rake security:tag[1.1.1]"
          ],
          "extends" => ".with-bundle"
        },
        "security_release_tag:1.2.3" => {
          "image" => "$CI_REGISTRY_IMAGE/base:$CI_DEFAULT_BRANCH",
          "stage" => "security_release:tag:1.2.3",
          "script" => [
            "bundle exec rake security:tag[1.2.3]"
          ],
          "extends" => ".with-bundle"
        }
      }
    end

    let(:tag_jobs) { instance_double(Security::TagJobs) }

    before do
      allow(ENV).to receive(:[]).and_call_original
      allow(ENV).to receive(:[]).with('VERSIONS').and_return(versions.join(','))

      allow(Security::TagJobs).to receive(:new).with(versions).and_return(tag_jobs)
      allow(tag_jobs).to receive(:generate).and_return(expected_yaml.to_yaml)

      allow(File).to receive(:write)
    end

    it 'reads the VERSIONS environment variable and splits it into an array' do
      expect(ENV).to receive(:[]).with('VERSIONS').and_return(versions.join(','))
      task.invoke
    end

    it 'calls generate method on the Security::TagJobs instance' do
      expect(Security::TagJobs).to receive(:new).with(versions).and_return(tag_jobs)
      expect(tag_jobs).to receive(:generate)
      task.invoke
    end

    it 'writes the generated YAML content to dynamic-tag-gitlab-ci.yml' do
      expect(File).to receive(:write).with('dynamic-tag-gitlab-ci.yml', tag_jobs.generate)
      task.invoke
    end
  end
end
