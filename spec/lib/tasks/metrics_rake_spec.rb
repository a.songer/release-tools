# frozen_string_literal: true

require 'rake_helper'

describe 'metrics tasks', :rake do
  describe 'release_status', task: 'metrics:release_status' do
    it 'updates the release status metrics' do
      expect(ReleaseTools::Metrics::ReleaseMetricUpdater).to receive(:new)
        .and_return(instance_double(ReleaseTools::Metrics::ReleaseMetricUpdater, execute: true))

      task.invoke
    end
  end

  describe 'monthly_release:announced', task: 'metrics:monthly_release:announced' do
    it 'creates a monthly release status metric with announced status' do
      expect(ReleaseTools::Metrics::MonthlyReleaseStatus).to receive(:new).with(status: :announced)
        .and_return(instance_double(ReleaseTools::Metrics::MonthlyReleaseStatus, execute: true))

      task.invoke
    end
  end

  describe 'patch_release:open', task: 'metrics:patch_release:open' do
    it 'creates a patch release status metric with open status' do
      expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :open)
        .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

      task.invoke
    end
  end

  describe 'patch_release:warning', task: 'metrics:patch_release:warning' do
    it 'creates a patch release status metric with warning status' do
      expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :warning)
        .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

      task.invoke
    end
  end
end
