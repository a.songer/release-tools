# frozen_string_literal: true

require 'rake_helper'

describe 'release environment tasks', :rake do
  describe 'create', task: 'release_environment:create' do
    let(:environment) do
      instance_double(ReleaseTools::ReleaseEnvironment::Environment, create: nil)
    end

    it 'calls the Environment class' do
      expect(ReleaseTools::ReleaseEnvironment::Environment)
        .to receive(:new)
        .with('16.8.0')
        .and_return(environment)

      expect(environment).to receive(:create)

      task.invoke('16.8.0')
    end
  end

  describe 'notify', task: 'release_environment:notify' do
    let(:notifier) do
      instance_double(ReleaseTools::ReleaseEnvironment::DeployNotifier, execute: nil)
    end

    it 'calls the Notifier class' do
      expect(ReleaseTools::ReleaseEnvironment::DeployNotifier)
        .to receive(:new)
        .with(
          pipeline_id: 123,
          environment_name: "my_env",
          release_environment_version: "my_version"
        )
        .and_return(notifier)

      expect(notifier).to receive(:execute)

      task.invoke(
        123,
        "my_env",
        "my_version"
      )
    end
  end
end
