# frozen_string_literal: true

require 'rake_helper'

describe 'monthly tasks', :rake do
  describe 'finalize:start', task: 'monthly:finalize:start' do
    it 'starts the finalize stage' do
      expect(ReleaseTools::Slack::ReleasePipelineStartNotifier).to receive(:new).with(release_type: :monthly, stage: :finalize)
        .and_return(instance_double(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches, execute: true))

      task.invoke
    end
  end

  describe 'update_protected_branches', task: 'monthly:finalize:update_protected_branches' do
    it 'disables omnibus nightly' do
      expect(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches).to receive(:new)
        .and_return(instance_double(ReleaseTools::Monthly::Finalize::UpdateProtectedBranches, execute: true))

      task.invoke
    end
  end

  describe 'finalize:create_release_status_metric', task: 'monthly:finalize:create_release_status_metric' do
    it 'creates a new release status metric with open status' do
      expect(ReleaseTools::Metrics::MonthlyReleaseStatus).to receive(:new).with(status: :open)
        .and_return(instance_double(ReleaseTools::Metrics::MonthlyReleaseStatus, execute: true))

      task.invoke
    end
  end

  describe 'finalize:create_version', task: 'monthly:finalize:create_version' do
    it 'creates a new version on version.gitlab.com' do
      expect(ReleaseTools::Monthly::Finalize::CreateVersion).to receive(:new)
        .and_return(instance_double(ReleaseTools::Monthly::Finalize::CreateVersion, execute: true))
      task.invoke
    end
  end
end
