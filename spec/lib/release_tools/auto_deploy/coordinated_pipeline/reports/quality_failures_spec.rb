# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::QualityFailures do
  let(:ops_client) { stub_const('ReleaseTools::GitlabOpsClient', spy) }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:coordinated_pipeline) { create(:pipeline) }
  let(:pipelines) do
    [
      double(:pipeline,
             web_url: 'example.com',
             jobs: [double(:job, title: 'foo', web_url: 'job-example.com', errors: [])])
    ]
  end

  let(:fetcher) do
    stub_const('ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Fetcher', spy)
  end

  subject(:report) { described_class.new(environment: 'gstg-cny', coordinated_pipeline_id: '123') }

  before do
    allow(ReleaseTools::AutoDeploy::CoordinatedPipeline::Reports::Quality::Fetcher)
      .to receive(:new)
      .and_return(fetcher)

    allow(ops_client)
      .to receive(:pipeline)
      .and_return(coordinated_pipeline)

    allow(fetcher)
      .to receive(:execute)
      .and_return(pipelines)
  end

  describe '#create' do
    it 'creates an issue' do
      allow(report)
        .to receive(:assignees)
        .and_return([1, 2])

      expect(client)
        .to receive(:create_issue)
        .with(
          report,
          ReleaseTools::Project::Release::Tasks
        )

      without_dry_run { report.create }
    end

    context 'with no pipelines' do
      let(:pipelines) { [] }

      it 'raises an error' do
        expect { report.create }.to raise_error(described_class::NoFailuresFoundError)
      end
    end

    context 'with dry run' do
      it 'does nothing' do
        allow(report)
          .to receive(:assignees)
          .and_return([1, 2])

        expect(client).not_to receive(:create_issue)

        report.create
      end
    end
  end

  describe '#title' do
    context 'with multiple pipelines' do
      let(:pipelines) { create_list(:pipeline, 2) }

      it 'returns a general title' do
        expect(report.title).to eq('QA failures on gstg-cny')
      end
    end

    context 'with a single pipeline' do
      it 'returns the spec name' do
        expect(report.title).to eq('QA failure: foo')
      end
    end
  end

  describe '#description' do
    it 'returns the report content' do
      expect(report.description).to be_present
    end
  end

  describe '#assignees' do
    it 'returns the assignee IDs' do
      schedule = instance_spy(ReleaseTools::ReleaseManagers::Schedule)

      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      expect(schedule)
        .to receive(:active_release_managers)
        .and_return([double('foo', id: 1), double('bar', id: 2)])

      expect(report.assignees).to eq([1, 2])
    end
  end
end
