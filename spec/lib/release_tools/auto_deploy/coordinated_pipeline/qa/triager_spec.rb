# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::AutoDeploy::CoordinatedPipeline::Qa::Triager do
  subject(:triager) { described_class.new(deploy_version: deploy_version, environment: environment) }

  let(:deploy_version) { '17.1.202406041400-696dc61a063.1888b47a80d' }
  let(:environment) { 'gstg-cny' }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:qa_issue) { create(:issue, labels: ['release-blocker']) }

  describe '#execute' do
    before do
      enable_feature(:quality_triager)

      allow(client)
        .to receive(:issues)
        .and_return([qa_issue])
    end

    it 'adds labels and closes the issue' do
      expect(client)
        .to receive(:create_issue_note)
        .with(
          ReleaseTools::Project::Release::Tasks,
          { issue: qa_issue, body: instance_of(String) }
        )

      expect(client).to receive(:edit_issue)
      expect(client).to receive(:close_issue)

      without_dry_run do
        triager.execute
      end
    end

    context 'when the issue is not found' do
      let(:qa_issue) { nil }

      it 'does nothing' do
        expect(client).not_to receive(:create_issue_note)
        expect(client).not_to receive(:edit_issue)
        expect(client).not_to receive(:close_issue)

        without_dry_run do
          triager.execute
        end
      end
    end

    context 'when the feature flag is disabled' do
      it 'does nothing' do
        disable_feature(:quality_triager)

        expect(client).not_to receive(:create_issue_note)
        expect(client).not_to receive(:edit_issue)
        expect(client).not_to receive(:close_issue)

        without_dry_run do
          triager.execute
        end
      end
    end

    context 'when running in dry-run mode' do
      it 'does nothing' do
        disable_feature(:quality_triager)

        expect(client).not_to receive(:create_issue_note)
        expect(client).not_to receive(:edit_issue)
        expect(client).not_to receive(:close_issue)

        triager.execute
      end
    end
  end
end
