# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::MergeWhenPipelineSucceedsService do
  let(:merge_request) { ReleaseTools::MergeRequest.new }
  let(:token) { 'a token' }
  let(:approve_service) { instance_double(ReleaseTools::Services::ApproveService) }

  subject(:service) { described_class.new(merge_request, token: token) }

  before do
    allow(ReleaseTools::Services::ApproveService).to receive(:new).and_return(approve_service)
  end

  describe '.new' do
    it 'initialize a Gitlab::Client with the given token and endpoint' do
      endpoint = 'https://gitlab.example.com'
      client = class_double(Gitlab::Client).as_stubbed_const
      expect(client)
        .to receive(:new)
        .with(
          {
            private_token: token,
            endpoint: endpoint,
            httparty: a_hash_including(logger: instance_of(SemanticLogger::Logger))
          }
        )
        .and_return(double(:client, user: double(:user, id: 2)))

      allow(merge_request).to receive(:author).and_return(double(:user, id: 1))
      described_class.new(merge_request, token: token, endpoint: endpoint)
    end
  end

  describe '#execute' do
    context 'when the pipeline is ready' do
      it 'approves it, wait for the pipeline to start, and accept the merge request' do
        expect(approve_service).to receive(:execute)
        expect(service).to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          service.execute
        end
      end
    end

    context 'when we have a connection problem' do
      it 'does retry' do
        # first run
        expect(approve_service).to receive(:execute).and_raise(gitlab_error(:BadRequest))

        # second run
        expect(approve_service).to receive(:execute)
        expect(service).to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          service.execute
        end
      end
    end
  end

  describe '#merge_when_pipeline_succeeds' do
    it 'delegates to Gitlab::Client#accept_merge_request' do
      allow(merge_request).to receive_messages(iid: double(:iid), project_id: double(:project_id))

      expect(service.client)
        .to receive(:accept_merge_request)
        .with(
          merge_request.project_id,
          merge_request.iid,
          {
            merge_when_pipeline_succeeds: true,
            should_remove_source_branch: true
          }
        )

      without_dry_run do
        service.merge_when_pipeline_succeeds
      end
    end
  end
end
