# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Services::AutoMergeService do
  let(:merge_request) { double('ReleaseTools::MergeRequest', project_id: 42, project: double, iid: 12, url: 'a url') }
  let(:token) { 'a token' }

  subject(:service) { described_class.new(merge_request, token: token) }

  describe '#execute' do
    let(:new_pipeline) do
      double(
        :pipeline,
        id: 1234,
        web_url: 'https://test.com/security/gitlab/-/pipelines/123',
        status: 'running'
      )
    end

    let(:old_pipeline) do
      double(
        :pipeline,
        id: 1234,
        web_url: 'https://test.com/security/gitlab/-/pipelines/123',
        status: 'success'
      )
    end

    before do
      allow(ReleaseTools::GitlabClient)
        .to receive(:create_merge_request_pipeline)
        .and_return(new_pipeline)

      allow(ReleaseTools::GitlabClient)
        .to receive(:pipelines)
        .and_return([old_pipeline], [new_pipeline])
    end

    it 'waits for the pipeline to start' do
      expect(service).to receive(:wait_for_mr_pipeline_to_start)
      expect(service).to receive(:merge_when_pipeline_succeeds)

      without_dry_run do
        service.execute
      end
    end

    context 'when the pipeline is not ready' do
      let(:new_pipeline) do
        double(
          :pipeline,
          id: 1234,
          web_url: 'https://test.com/security/gitlab/-/pipelines/123',
          status: 'created'
        )
      end

      it 'does not add to merge train' do
        expect(service).not_to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          expect { service.execute }.to raise_error(described_class::PipelineNotReadyError)
        end
      end
    end

    context 'when pipeline already exists' do
      let(:old_pipeline) do
        double(
          :pipeline,
          id: 1234,
          web_url: 'https://test.com/security/gitlab/-/pipelines/123',
          status: 'running'
        )
      end

      it 'does not trigger new pipeline' do
        expect(ReleaseTools::GitlabClient)
          .not_to receive(:create_merge_request_pipeline)

        expect(service).to receive(:merge_when_pipeline_succeeds)

        without_dry_run do
          service.execute
        end
      end
    end
  end

  describe '#merge_when_pipeline_succeeds' do
    it 'set MWPS with the gitlab_bot_token' do
      sub_service = double('sub-service')
      allow(ReleaseTools::Services::MergeWhenPipelineSucceedsService)
        .to receive(:new)
        .with(merge_request, { token: token })
        .and_return(sub_service)

      expect(sub_service).to receive(:execute)

      service.__send__(:merge_when_pipeline_succeeds)
    end
  end
end
