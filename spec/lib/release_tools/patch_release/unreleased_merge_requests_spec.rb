# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PatchRelease::UnreleasedMergeRequests do
  let(:version) { ReleaseTools::Version.new('15.5.5') }
  let(:project) { ReleaseTools::Project::GitlabEe }

  let(:commit_list) { create_list(:commit, 4) }

  subject(:merge_requests) do
    described_class.new(version, project)
  end

  describe '#execute' do
    it 'returns a deduplicated list of unreleased merge requests' do
      commits = double(ReleaseTools::PatchRelease::UnreleasedCommits, execute: commit_list)

      client_payload1 = [{ 'id' => 1, 'title' => 'foo' }]
      client_payload2 = [{ 'id' => 2, 'title' => 'bar' }]
      client_payload3 = [{ 'id' => 1, 'title' => 'foo' }]
      client_payload4 = []

      expected_output = [
        { 'id' => 1, 'title' => 'foo' },
        { 'id' => 2, 'title' => 'bar' }
      ]

      allow(ReleaseTools::PatchRelease::UnreleasedCommits).to receive(:new).and_return(commits)

      expect(ReleaseTools::GitlabClient)
        .to receive(:commit_merge_requests)
        .and_return(client_payload1)

      expect(ReleaseTools::GitlabClient)
        .to receive(:commit_merge_requests)
        .and_return(client_payload2)

      expect(ReleaseTools::GitlabClient)
        .to receive(:commit_merge_requests)
        .and_return(client_payload3)

      expect(ReleaseTools::GitlabClient)
        .to receive(:commit_merge_requests)
        .and_return(client_payload4)

      expect(merge_requests.execute).to eq(expected_output)
    end
  end
end
