# frozen_string_literal: true

require 'spec_helper'
require 'rake_helper'

RSpec.describe ReleaseTools::Security::TagPatchRelease do
  let(:version) { '1.2.3' }
  let(:tag_patch_release) { described_class.new(version) }
  let(:notifier) { instance_double(ReleaseTools::Slack::ReleaseJobEndNotifier, send_notification: true) }
  let(:logger) { instance_double(SemanticLogger::Logger) }
  let(:exception) { StandardError.new('Something went wrong') }

  before do
    allow(ReleaseTools::Slack::ReleaseJobEndNotifier)
      .to receive(:new)
      .and_return(notifier)

    allow(described_class).to receive(:logger).and_return(logger)
  end

  describe '#initialize' do
    it 'sets the version' do
      expect(tag_patch_release.instance_variable_get(:@version)).to eq(version)
    end
  end

  describe '#execute' do
    context 'when successful' do
      it 'sends a success notification' do
        expect(tag_patch_release).to receive(:send_slack_notification).with(:success)
        expect(Rake::Task).to receive(:[]).with('release:tag').and_return(instance_double(Rake::Task, invoke: true))
        tag_patch_release.execute
      end
    end

    context 'when an error occurs' do
      it 'sends a failed notification and logs the expected error message' do
        expect(tag_patch_release).to receive(:send_slack_notification).with(:failed)

        error_message = "Tagging the patch release failed with error: #{exception.message}.\nReview the error log and consider retrying this job."
        expect(logger).to receive(:fatal).with(include(error_message), error: exception)

        allow(Rake::Task).to receive(:[]).with('release:tag').and_raise(exception)

        expect { tag_patch_release.execute }.to raise_error(StandardError)
      end
    end

    it 'sends a notification with the correct parameters' do
      allow(Rake::Task)
        .to receive(:[])
        .with('release:tag')
        .and_return(instance_double(Rake::Task, invoke: true))

      expect(ReleaseTools::Slack::ReleaseJobEndNotifier)
        .to receive(:new)
        .with(job_type: "Tag patch release version #{version}", status: :success, release_type: :patch)
        .and_return(notifier)

      expect(notifier).to receive(:send_notification)

      tag_patch_release.execute
    end
  end
end
