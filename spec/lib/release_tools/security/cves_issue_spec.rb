# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::CvesIssue do
  let(:issue) do
    build(
      :issue,
      title: 'Bypass some auth',
      description: File.read('spec/fixtures/cve_issue_description.md'),
      labels: ['foo', 'cve::1234-1234', 'cve']
    )
  end

  subject { described_class.new(issue) }

  it 'returns title' do
    expect(subject.title).to eq('Bypass some auth')
  end

  it 'returns cvss_string' do
    expect(subject.cvss_string).to eq('CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N')
  end

  it 'returns impact' do
    expect(subject.impact).to eq('AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N')
  end

  it 'returns vulnerability_description' do
    expect(subject.vulnerability_description).to eq('Vulnerability description')
  end

  it 'returns credit' do
    expect(subject.credit).to eq('Thanks [someone](https://example.com/foo) for reporting this vulnerability through our bug bounty program')
  end

  it 'returns yaml' do
    expect(subject.yaml).to include(
      {
        'reporter' => { 'name' => 'GitLab Security Team', 'email' => 'security@example.com' },
        'vulnerability' => include({ 'impact' => 'AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N' })
      }
    )
  end

  context 'when yaml is not present' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: '') }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlMissingError, 'CVE issue does not have YAML')
    end
  end

  context 'when yaml is not a Hash' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: "```yaml\na\n```") }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlInvalidError, 'CVE issue YAML is not a Hash')
    end
  end

  context 'when yaml loading fails' do
    let(:issue) { build(:issue, title: 'Bypass some auth', description: "```yaml\na: something, b: other\n```") }

    it 'raises error' do
      expect { subject.yaml }.to raise_error(described_class::YamlParseError, 'CVE issue contains invalid YAML')
    end
  end

  describe '#cvss_severity' do
    it 'returns cvss severity' do
      expect(subject.cvss_severity).to eq('High')
    end
  end

  describe '#cvss_base_score' do
    it 'returns cvss base_score' do
      expect(subject.cvss_base_score).to eq(7.6)
    end
  end

  describe '#cve_id' do
    it 'finds the cve id from the issue labels' do
      expect(subject.cve_id).to eq('CVE-1234-1234')
    end

    context 'no id present' do
      it 'returns nil' do
        expect(issue).to receive(:labels).and_return([])

        expect(subject.cve_id).to be_nil
      end
    end
  end

  describe '#yaml_str' do
    it 'returns the YAML definition' do
      expect(subject.yaml_str).to include('description: Vulnerability description')
    end
  end

  describe '#yaml_present?' do
    context 'with YAML present' do
      it 'returns true' do
        expect(subject).to be_yaml_present
      end
    end

    context 'without YAML present' do
      let(:issue) do
        build(
          :issue,
          title: 'Bypass some auth',
          description: '',
          labels: ['foo', 'cve::1234-1234', 'cve']
        )
      end

      it 'returns false' do
        expect(subject).not_to be_yaml_present
      end
    end
  end

  describe '#valid_yaml?' do
    context 'with valid YAML' do
      it 'returns true' do
        expect(subject).to be_valid_yaml
      end
    end

    context 'with invalid YAML' do
      let(:issue) do
        build(
          :issue,
          title: 'Bypass some auth', description: "```yaml\na: something, b: other\n```"
        )
      end

      it 'returns false' do
        expect(subject).not_to be_valid_yaml
      end
    end
  end

  describe '#web_url' do
    it 'returns the web url of the CVES issue' do
      expect(subject.web_url).to eq(issue.web_url)
    end
  end

  describe '#affected_versions' do
    it 'returns the versions listed' do
      expect(subject.affected_versions).to eq(['>=1X.X, <16.X.X', '>=16.Y, <16.Y.Y', '>=16.Z, <16.Z.Z'])
    end
  end

  describe '#pending_affected_versions?' do
    it { is_expected.not_to be_pending_affected_versions }

    context 'with invalid CVES' do
      let(:issue) do
        build(:issue, description: File.read('spec/fixtures/incomplete_cves_issue.md'))
      end

      it { is_expected.to be_pending_affected_versions }
    end
  end

  describe '#fixed_versions' do
    it 'returns the versions listed' do
      expect(subject.fixed_versions).to eq(['16.X.X', '16.Y.Y', '16.Z.Z'])
    end
  end

  describe '#pending_fixed_versions?' do
    it { is_expected.not_to be_pending_fixed_versions }

    context 'with invalid CVES' do
      let(:issue) do
        build(:issue, description: File.read('spec/fixtures/incomplete_cves_issue.md'))
      end

      it { is_expected.to be_pending_fixed_versions }
    end
  end
end
