# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::TargetIssueNotifier do
  let(:client) { ReleaseTools::GitlabClient }
  let(:event) { :linked }
  let(:issue) { create(:security_implementation_issue) }
  let(:tracking_issue) { create(:issue) }
  let(:notifier) { described_class.new(event, issue) }

  before do
    allow(client).to receive(:next_security_tracking_issue).and_return(tracking_issue)

    allow(issue).to receive(:pending_reasons).and_return(%w(a b))
  end

  describe '.notify' do
    it 'creates a new instance and calls execute' do
      event = :linked
      issue = double(:issue)
      notifier = double(:notifier)

      expect(described_class).to receive(:new).with(event, issue).and_return(notifier)
      expect(notifier).to receive(:execute).and_return(true)

      described_class.notify(event, issue)
    end
  end

  describe '#execute' do
    it 'does not post a comment in a dry run' do
      expect(client).not_to receive(:create_issue_note)

      notifier.execute
    end

    context 'with no assignees' do
      it 'posts a comment pinging the author' do
        allow(issue).to receive(:author)
          .and_return(double(:user, username: 'foo'))

        expect(client).to receive(:create_issue_note).with(
          issue.project,
          issue: issue,
          body: /@foo, this issue has been linked.*#{tracking_issue.web_url}/
        ).and_return(true)

        without_dry_run do
          notifier.execute
        end
      end
    end

    context 'with assignees' do
      it 'posts a comment to the tracking issue' do
        allow(issue).to receive(:assignees)
          .and_return([double(:user, username: 'foo'), double(:user, username: 'bar')])

        expect(client).to receive(:create_issue_note).with(
          issue.project,
          issue: issue,
          body: /@foo, @bar, this issue has been linked.*#{tracking_issue.web_url}/
        ).and_return(true)

        without_dry_run do
          notifier.execute
        end
      end
    end

    context 'unlink event' do
      let(:event) { :unlinked }

      it 'posts a comment to the tracking issue' do
        allow(issue).to receive(:assignees)
          .and_return([double(:user, username: 'foo'), double(:user, username: 'bar')])

        expect(client).to receive(:create_issue_note).with(
          issue.project,
          issue: issue,
          body: /@foo, @bar, this issue was unlinked.*#{tracking_issue.web_url}.*\n\n- a\n- b/
        ).and_return(true)

        without_dry_run do
          notifier.execute
        end
      end
    end

    context 'not ready event' do
      let(:event) { :not_ready }

      it 'posts a comment to the tracking issue' do
        allow(issue).to receive(:assignees)
          .and_return([double(:user, username: 'foo'), double(:user, username: 'bar')])

        expect(client).to receive(:create_issue_note).with(
          issue.project,
          issue: issue,
          body: /@foo, @bar, this issue was reviewed to be included.*#{tracking_issue.web_url}.*\n\n- a\n- b/
        ).and_return(true)

        without_dry_run do
          notifier.execute
        end
      end
    end
  end

  context 'with an invalid event' do
    let(:event) { :foo }

    it 'raises an error' do
      without_dry_run do
        expect { notifier.execute }.to raise_error(described_class::InvalidEvent, "foo is not a valid event type.")
      end
    end
  end
end
