# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::Prepare::ComponentBranchVerifier do
  describe '#execute' do
    let(:ee_project) { double(:ee_project, metadata_project_name: 'gitlab-ee', path: 'gitlab', ee_branch?: true, default_branch: 'master') }
    let(:project) { double(:project, metadata_project_name: 'project', path: 'project', ee_branch?: false, default_branch: 'main') }
    let(:projects) { [ee_project, project] }
    let(:versions) { [double(stable_branch: '1.0'), double(stable_branch: '2.0')] }

    subject(:instance_verifier) { described_class.new }

    before do
      allow(ReleaseTools::Versions).to receive(:next_versions).and_return(versions)
      allow(instance_verifier).to receive(:projects).and_return(projects)
    end

    context 'when all statuses are success' do
      it 'outputs a success message and notifies slack' do
        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: 'master',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: '1.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: '2.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: 'main',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: '1.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: '2.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          job_type: 'Component status',
          status: :success,
          release_type: :patch
        ).and_return(double(send_notification: true))

        expect(instance_verifier.logger).to receive(:info).with(/All components are green/)

        without_dry_run do
          instance_verifier.execute
        end
      end
    end

    context 'when some statuses are not success and raises an exception' do
      it 'outputs the component versions that are not successful' do
        logger_output = "

❌ The following components do not have green pipelines:

- gitlab-ee - master - failed: https://gitlab.com/gitlab/-/commits/master
- gitlab-ee - 2.0 - skipped: https://gitlab.com/gitlab/-/commits/2.0
- project - 2.0 - running: https://gitlab.com/project/-/commits/2.0"

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: 'master',
          commit_depth: 20
        ).and_return(double(execute: 'failed'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: '1.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: ee_project,
          branch: '2.0',
          commit_depth: 20
        ).and_return(double(execute: 'skipped'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: 'main',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: '1.0',
          commit_depth: 20
        ).and_return(double(execute: 'success'))

        expect(ReleaseTools::Services::ComponentStatusService).to receive(:new).with(
          project: project,
          branch: '2.0',
          commit_depth: 20
        ).and_return(double(execute: 'running'))

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          job_type: 'Component status',
          status: :failed,
          release_type: :patch
        ).and_return(double(send_notification: true))

        expect(instance_verifier.logger).to receive(:info).with(logger_output)

        without_dry_run do
          expect { instance_verifier.execute }.to raise_error(described_class::CouldNotCompleteError)
        end
      end
    end

    context 'when the job is unable to complete and an error is raised' do
      it 'outputs manual instructions and raises an error' do
        log_message = "Component status check failed. If this job continues to fail, the status of the projects can be checked manually:\n\n- project_name - branch_name: link"

        allow(ReleaseTools::Services::ComponentStatusService).to receive(:new)
          .and_raise(Gitlab::Error::Error)

        expect(instance_verifier).to receive(:generate_link_list)
          .with(projects, versions)
          .and_yield('project_name', 'branch_name', 'link')

        expect(ReleaseTools::Slack::ReleaseJobEndNotifier).to receive(:new).with(
          job_type: 'Component status',
          status: :failed,
          release_type: :patch
        ).and_return(double(send_notification: true))

        expect(instance_verifier.logger).to receive(:fatal).with(log_message, error: anything)

        without_dry_run do
          expect { instance_verifier.execute }.to raise_error(described_class::CouldNotCompleteError)
        end
      end
    end
  end
end
