# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::TrackingIssue do
  before do
    allow(ReleaseTools::GitlabReleasesClient)
      .to receive_messages(
        current_minor_for_date: '16.7',
        previous_minors: ['16.7', '16.6', '16.5'],
        next_patch_release_date: '2023-03-27'
      )
  end

  subject { described_class.new }

  describe '#title' do
    subject { described_class.new.title }

    it { is_expected.to eq('Patch release: 16.7.x, 16.6.x, 16.5.x') }

    context 'when performing a critical patch release' do
      subject do
        ClimateControl.modify(SECURITY: 'critical') do
          described_class.new.title
        end
      end

      it { is_expected.to eq('Critical patch release: 16.7.x, 16.6.x, 16.5.x') }
    end
  end

  describe '#confidential?' do
    it { is_expected.to be_confidential }
  end

  describe '#labels' do
    subject { described_class.new.labels }

    it { is_expected.to include('upcoming security release') }
    it { is_expected.to include('security') }
    it { is_expected.to include('meta') }
  end

  describe '#project' do
    subject { described_class.new.project }

    it { is_expected.to eq(ReleaseTools::Project::GitlabEe) }
  end

  describe '#versions_title' do
    it 'includes the upcoming versions for the release cadence' do
      expect(subject.versions_title).to eq('16.7.x, 16.6.x, 16.5.x')
    end
  end

  describe '#description' do
    it 'returns the tracking issue template' do
      expect(subject.description).not_to be_nil
    end
  end

  describe '#due_date' do
    it 'calls the gitlab-releases client' do
      expect(ReleaseTools::GitlabReleasesClient)
        .to receive(:next_patch_release_date)

      subject.due_date
    end

    it 'uses the next patch release date' do
      expect(subject.due_date).to eq('2023-03-27')
    end
  end

  describe '#assignees' do
    let(:schedule) { instance_spy(ReleaseTools::ReleaseManagers::Schedule) }

    it 'returns assignees ids of the scheduled release managers' do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)

      allow(ReleaseTools::GitlabReleasesClient)
        .to receive(:version_for_date)
        .and_return('16.1')

      expect(schedule)
        .to receive(:authorized_release_managers)
        .with('16.1.0')
        .and_return([double('user3', id: 3), double('user4', id: 4)])

      expect(subject.assignees).to eq([3, 4])
    end
  end
end
