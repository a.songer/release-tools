# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockersReport do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:blockers) { stub_const('ReleaseTools::Deployments::Blockers', spy) }

  let(:blocker1) { double(:blocker) }
  let(:blocker2) { double(:blocker) }

  let(:start_time) { Date.new(2024, 4, 22) }
  let(:end_time) { Date.new(2024, 4, 28) }

  subject(:report) do
    described_class.new
  end

  before do
    allow(blockers)
      .to receive(:deployment_blockers)
      .and_return([blocker1, blocker2])
  end

  describe '#create' do
    before do
      allow(blockers)
        .to receive(:fetch)
        .and_return(nil)

      allow(client)
        .to receive(:create_deployment_blockers_report)
        .and_return(nil)

      allow(report)
        .to receive(:assignees)
        .and_return([1, 2])
    end

    it 'creates an issue' do
      expect(client)
        .to receive(:create_issue)
        .with(
          report,
          ReleaseTools::Project::Release::Tasks
        )

      report.create
    end
  end

  describe '#assignees' do
    let(:schedule) do
      instance_double(
        ReleaseTools::ReleaseManagers::Schedule,
        active_release_managers: [create(:user, id: 1), create(:user, id: 2)]
      )
    end

    before do
      allow(ReleaseTools::ReleaseManagers::Schedule)
        .to receive(:new)
        .and_return(schedule)
    end

    it 'returns the current RM user IDs' do
      expect(report.assignees).to eq([1, 2])
    end

    context 'when VersionNotFoundError is raised' do
      let(:schedule_with_error) do
        instance_double(
          ReleaseTools::ReleaseManagers::Schedule,
          active_release_managers: [],
          active_appsec_release_managers: []
        )
      end

      before do
        allow(ReleaseTools::ReleaseManagers::Schedule)
          .to receive(:new)
          .and_return(schedule_with_error)

        allow(schedule_with_error)
          .to receive(:active_release_managers)
          .and_raise(ReleaseTools::ReleaseManagers::Schedule::VersionNotFoundError)
      end

      it 'returns nil' do
        expect(report.assignees).to be_nil
      end
    end
  end

  describe '#title' do
    it 'returns the title of the report' do
      allow(report).to receive_messages(
        start_time: start_time,
        end_time: end_time
      )
      expect(report.title).to eq("Deployment Blockers - Week: #{start_time}-#{end_time}")
    end
  end

  describe '#description' do
    let(:blocker4) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 4',
        root_cause: "~\"RootCause::Flaky-Test\"",
        data: double('data', labels: ["Deploys-blocked-gstg::5.0hr", "Deploys-blocked-gprd::3.0hr"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker5) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 5',
        root_cause: "~\"RootCause::Feature-Flag\"",
        data: double('data', labels: ["Deploys-blocked-gstg::1.0hr", "Deploys-blocked-gprd::4.0hr"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker6) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 6',
        root_cause: "~\"RootCause::Flaky-Test\"",
        data: double('data', labels: ["Deploys-blocked-gstg::1.0", "Deploys-blocked-gprd::0.5"]),
        with_deploys_blocked_label?: true
      )
    end

    let(:blocker7) do
      instance_double(
        ReleaseTools::Deployments::BlockerIssue,
        title: 'Blocker 7'
      )
    end

    let(:blocker_issue_fetcher) { instance_double(ReleaseTools::Deployments::BlockerIssueFetcher) }

    before do
      allow(blocker4).to receive_messages(
        hours_gstg_blocked: 5.0,
        hours_gprd_blocked: 3.0
      )

      allow(blocker5).to receive_messages(
        hours_gstg_blocked: 1.0,
        hours_gprd_blocked: 4.0
      )

      allow(blocker6).to receive_messages(
        hours_gstg_blocked: 1.0,
        hours_gprd_blocked: 0.5
      )

      allow(ReleaseTools::Deployments::BlockerIssueFetcher).to receive(:new).and_return(blocker_issue_fetcher)
      allow(blocker_issue_fetcher).to receive(:fetch)
    end

    it 'returns the content of the report' do
      allow(report).to receive_messages(
        start_time: start_time,
        end_time: end_time,
        deployment_blockers: [blocker4, blocker5, blocker6],
        uncategorized_incidents: [blocker7]
      )

      expected_content = File.read('spec/fixtures/reports/deployment_blockers.html.md')

      expect(report.description).to eq(expected_content)
    end
  end
end
