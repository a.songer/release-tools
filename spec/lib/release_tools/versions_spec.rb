# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Versions do
  describe '.current' do
    it 'returns versions from version.gitlab.com', vcr: { cassette_name: 'versions/list' } do
      expect(described_class.current).to include('16.10.6', '16.11.3', '17.0.1')
    end
  end

  describe '.next' do
    it 'returns an Array of the next patch versions' do
      versions = %w[1.0.0 1.1.0 1.1.1 1.2.3]

      expect(described_class.next(versions)).to eq(%w[1.0.1 1.1.1 1.1.2 1.2.4])
    end
  end

  describe '.latest' do
    it 'returns the latest versions grouped by minor version' do
      versions = %w[1.0.0 1.1.0 1.1.1 1.2.3]

      expect(described_class.latest(versions, 2)).to eq(%w[1.2.3 1.1.1])
    end
  end

  describe '.next_versions' do
    it 'returns the next patch versions of the latest releases', vcr: { cassette_name: 'versions/list' } do
      expect(described_class.next_versions)
        .to contain_exactly('16.10.7', '16.11.4', '17.0.2')
    end
  end

  describe '.last_version_for_major' do
    it 'returns the last version for a major version', vcr: { cassette_name: 'versions/list' } do
      expect(described_class.last_version_for_major(16)).to eq('16.10.6')
    end
  end

  describe '.sort' do
    it 'returns the sorted version list without duplicates' do
      versions = %w[1.2.3 1.0.0 2.1.2 1.0.0 0.5.1]

      expect(described_class.sort(versions)).to eq(%w[0.5.1 1.0.0 1.2.3 2.1.2])
    end
  end

  describe '.current_stable_branch' do
    it 'returns the current version', vcr: { cassette_name: 'versions/list' } do
      expect(described_class.current_stable_branch).to eq('17-0-stable-ee')
    end
  end

  describe '.current_version' do
    it 'returns the current version', vcr: { cassette_name: 'versions/list' } do
      expect(described_class.current_version).to eq('17.0.0')
    end
  end
end
