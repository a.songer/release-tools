# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::VersionClient do
  it 'returns an Array of version entries', vcr: { cassette_name: 'versions/list' } do
    versions = described_class.versions

    expect(versions.length).to eq(90)
    expect(versions.first.version).to eq('16.10.6')
  end

  it 'creates new version', vcr: { cassette_name: 'versions/create' } do
    version = described_class.create_version('42.0.0')

    expect(version.version).to eq('42.0.0')
  end

  it 'gets single version', vcr: { cassette_name: 'versions/get' } do
    version = described_class.get_version('16.11.0')

    expect(version.version).to eq('16.11.0')
  end
end
