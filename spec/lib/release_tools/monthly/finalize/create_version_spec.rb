# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Monthly::Finalize::CreateVersion do
  let(:client) { ReleaseTools::VersionClient }
  let(:gitlab_releases) { GitlabReleases }
  let(:active_version) { "17.0" }
  let(:version) { "wrong_version" }
  let(:notifier) { stub_const('ReleaseTools::Slack::ReleaseJobEndNotifier', spy) }

  describe '#execute' do
    subject(:create_version) { described_class.new.execute }

    context 'creating version' do
      before do
        allow(gitlab_releases).to receive(:active_version).and_return(active_version)
        allow(client).to receive(:get_version).with(active_version).and_return({ version: active_version })
      end

      it 'creates version on version.gitlab.com' do
        expect(client).to receive(:get_version).once.with(active_version)
        expect(client).to receive(:create_version).once.with(active_version)
        expect(notifier).to receive(:send_notification)

        without_dry_run do
          create_version
        end
      end
    end

    context 'when something goes wrong' do
      it 'raises an error' do
        allow(client)
          .to receive(:get_version)
          .and_raise(StandardError)

        expect(notifier).to receive(:send_notification)
        without_dry_run do
          expect { create_version }.to raise_error(StandardError)
        end
      end
    end

    context 'dry run' do
      it 'does not create the version' do
        expect(client).not_to receive(:get_version)
        expect(client).not_to receive(:create_version)
        expect(notifier).not_to receive(:send_notification)

        create_version
      end
    end
  end
end
