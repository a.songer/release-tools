# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Project::Jihu do
  it_behaves_like 'project .remotes'
  it_behaves_like 'project .to_s'

  describe '.metadata_project_name' do
    it { expect(described_class.metadata_project_name).to eq('jihu') }
  end
end
