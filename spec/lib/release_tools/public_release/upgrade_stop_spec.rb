# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::UpgradeStop do
  subject(:upgrade_stop) { described_class.new }

  let(:upgrade_path_yaml) do
    <<~FILE
      ---
      - major: 30
        minor: 0
      - major: 40
        minor: 0
      - major: 40
        minor: 9
      - major: 40
        minor: 10
      - major: 40
        minor: 11
    FILE
  end

  describe "#last_required_stop" do
    using RSpec::Parameterized::TableSyntax

    where(:latest_released, :expected_stop) do
      "29.0.0" | nil
      "29.9.9" | nil
      "30.0.0" | "30.0"
      "30.0.1" | "30.0"
      "31.0.0" | "30.0"
      "39.9.9" | "30.0"
      "40.0.0" | "40.0"
      "40.0.1" | "40.0"
      "40.1.0" | "40.0"
      "40.8.0" | "40.0"
      "40.8.1" | "40.0"
      "40.10.0" | "40.10"
      "40.11.0" | "40.11"
      "40.12.0" | "40.11"
    end

    with_them do
      it "returns the expected upgrade stop" do
        allow(ReleaseTools::Versions)
          .to receive(:current_version)
          .and_return(ReleaseTools::Version.new(latest_released))

        allow(ReleaseTools::GitlabClient)
          .to receive(:get_file)
          .and_return(build(:gitlab_response, content: Base64.strict_encode64(upgrade_path_yaml)))

        expect(upgrade_stop.last_required_stop).to eq(expected_stop)
      end
    end
  end
end
