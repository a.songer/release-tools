# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'
require 'release_tools/project/infrastructure/production'

describe ReleaseTools::Tasks::AutoDeploy::CheckProduction do
  let(:deploy_version) { '' }
  let(:active_version) { ReleaseTools::Version.new('13.1') }
  let(:manager) { instance_double(ReleaseTools::Promotion::Manager) }

  subject(:task) { described_class.new }

  before do
    schedule =
      instance_double(ReleaseTools::ReleaseManagers::Schedule,
                      active_version: active_version)

    allow(ReleaseTools::ReleaseManagers::Schedule).to receive(:new).and_return(schedule)

    allow(ReleaseTools::Promotion::Manager).to receive(:new).and_return(manager)
  end

  around do |example|
    ClimateControl.modify(DEPLOY_VERSION: deploy_version) do
      in_coordinator_pipeline(&example)
    end
  end

  describe '#monthly_issue' do
    it 'returns the issue for the current milestone' do
      expect(ReleaseTools::MonthlyIssue).to receive(:new).with({ version: active_version })

      task.monthly_issue
    end
  end

  context 'when triggered by the deployer' do
    let(:deploy_version) { '13.1.202005220540-7c84ccdc806.59f00bb0515' }

    describe '#deploy_version' do
      it 'matches the environment variable' do
        expect(task.deploy_version).to eq(deploy_version)
      end
    end

    describe '#execute' do
      it 'attempts to authorize a deployment' do
        expect(manager).to receive(:authorize!).with(
          ReleaseTools::Version.new(deploy_version),
          ReleaseTools::MonthlyIssue
        )

        task.execute
      end
    end
  end

  describe '#deploy_version' do
    context 'when DEPLOY_VERSION is set' do
      let(:deploy_version) { '13.1.202005220540-7c84ccdc806.59f00bb0515' }

      it 'returns the env variable' do
        expect(task.deploy_version).to eq(deploy_version)
      end
    end

    context 'when DEPLOY_VERSION is not set' do
      let(:deploy_version) { nil }

      it 'returns the omnibus package' do
        fake_instance = double(:instance)
        omnibus_package = '13.1.202005220540-7c84ccdc806.59f00bb0515'

        allow(ReleaseTools::AutoDeploy::Tag)
          .to receive(:new)
          .and_return(fake_instance)

        allow(fake_instance)
          .to receive(:omnibus_package)
          .and_return(omnibus_package)

        expect(task.deploy_version).to eq(omnibus_package)
      end
    end
  end
end
