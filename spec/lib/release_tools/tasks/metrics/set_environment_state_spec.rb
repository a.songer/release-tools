# frozen_string_literal: true

require 'spec_helper'
require 'release_tools/tasks'

describe ReleaseTools::Tasks::Metrics::SetEnvironmentState do
  subject(:service) { described_class.new(environment, state) }

  let(:delivery_metrics) do
    instance_double(ReleaseTools::Metrics::Client)
  end

  let(:environment) { 'gstg-cny' }
  let(:state) { 'locked' }
  let(:env) { 'gstg' }
  let(:stage) { 'cny' }

  let(:environment_state_transition) { instance_spy(ReleaseTools::Metrics::EnvironmentStateTransition) }

  describe '#execute' do
    before do
      allow(ReleaseTools::Metrics::Client).to receive(:new).and_return(delivery_metrics)

      allow(ReleaseTools::Metrics::EnvironmentStateTransition)
        .to receive(:new)
        .and_return(environment_state_transition)

      %w[ready locked awaiting_promotion baking_time].each do |state|
        allow(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_environment_state',
            0,
            { labels: "#{state},#{env},#{stage}" }
          )
      end
    end

    it 'sets metric' do
      expect(delivery_metrics)
        .to receive(:set)
        .with(
          'auto_deploy_environment_state',
          1,
          { labels: "locked,#{env},#{stage}" }
        )

      %w[ready baking_time awaiting_promotion].each do |state|
        expect(delivery_metrics)
          .to receive(:set)
          .with(
            'auto_deploy_environment_state',
            0,
            { labels: "#{state},#{env},#{stage}" }
          )
      end

      without_dry_run { service.execute }
    end

    it 'calls EnvironmentStateTransition.valid? and sets metric if valid' do
      expect(ReleaseTools::Metrics::EnvironmentStateTransition)
        .to receive(:new)
        .with(env, stage, state)

      expect(environment_state_transition)
        .to receive(:valid?)
        .and_return(true)

      expect(delivery_metrics).to receive(:set)

      without_dry_run { service.execute }
    end

    it 'does not set metric if EnvironmentStateTransition.valid? is false' do
      expect(ReleaseTools::Metrics::EnvironmentStateTransition)
        .to receive(:new)
        .with(env, stage, state)

      expect(environment_state_transition)
        .to receive(:valid?)
        .and_return(false)

      expect(delivery_metrics).not_to receive(:set)

      without_dry_run { service.execute }
    end

    context 'when in dry_run mode' do
      it 'does not set metrics' do
        expect(ReleaseTools::Metrics::EnvironmentStateTransition).to receive(:new)
        expect(environment_state_transition).to receive(:valid?)
        expect(delivery_metrics).not_to receive(:set)

        service.execute
      end
    end
  end
end
