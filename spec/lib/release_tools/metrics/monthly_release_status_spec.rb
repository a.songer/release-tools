# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::MonthlyReleaseStatus do
  let(:delivery_metrics) { instance_double(ReleaseTools::Metrics::Client) }

  describe '#execute' do
    before do
      allow(ReleaseTools::Metrics::Client)
        .to receive(:new)
        .and_return(delivery_metrics)
    end

    context 'when the feature flag is disabled' do
      subject(:release_status) { described_class.new(status: :open) }

      it 'does nothing' do
        expect(delivery_metrics).not_to receive(:set)

        release_status.execute
      end
    end

    context 'when creating a new metric for the monthly release' do
      before do
        upcoming_releases = {
          '16.8' => '2024-01-18',
          '16.9' => '2024-02-15',
          '16.10' => '2024-03-21'
        }

        version = ReleaseTools::Version.new('16.9')

        allow(ReleaseTools::GitlabReleasesClient)
        .to receive_messages(upcoming_releases: upcoming_releases, version_for_date: version)

        enable_feature(:release_status_metric)
      end

      context 'when it is a dry run' do
        subject(:release_status) { described_class.new(status: :open) }

        it 'does nothing' do
          expect(delivery_metrics).not_to receive(:set)

          release_status.execute
        end
      end

      context 'when given an invalid status' do
        subject(:release_status) { described_class.new(status: :invalid) }

        it 'does nothing' do
          expect(delivery_metrics).not_to receive(:set)

          without_dry_run { release_status.execute }
        end
      end

      context 'when the status is open' do
        subject(:release_status) { described_class.new(status: :open) }

        it 'creates a status metric for the upcoming version' do
          expect(ReleaseTools::GitlabReleasesClient).to receive(:version_for_date)
            .with(Date.tomorrow)

          expect(delivery_metrics).to receive(:set)
            .with(described_class::METRIC, 1, { labels: "2024-02-15,16.9" })

          without_dry_run { release_status.execute }
        end
      end

      context 'when the status is announced' do
        subject(:release_status) { described_class.new(status: :announced) }

        it 'sets the value of the status metric for the next release version to 2 (announced)' do
          expect(delivery_metrics).to receive(:set)
           .with(described_class::METRIC, 2, { labels: "2024-02-15,16.9" })

          without_dry_run { release_status.execute }
        end
      end

      context 'when the status is tagged RC' do
        subject(:release_status) { described_class.new(status: :tagged_rc) }

        it 'sets the value of the status metric for the next release version to 3 (tagged RC)' do
          expect(delivery_metrics).to receive(:set)
           .with(described_class::METRIC, 3, { labels: "2024-02-15,16.9" })

          without_dry_run { release_status.execute }
        end
      end
    end
  end
end
