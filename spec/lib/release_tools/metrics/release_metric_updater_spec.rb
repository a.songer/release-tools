# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Metrics::ReleaseMetricUpdater do
  subject(:updater) { described_class.new }

  let(:delivery_prometheus) { instance_double(ReleaseTools::Prometheus::Query) }
  let(:monthly_version) { "16.9" }
  let(:next_patch_release_date) { "2024-02-15" }
  let(:current_minor_for_date) { "16.10" }
  let(:previous_minors) { ["16.9", "16.8", "16.7"] }

  describe '#execute' do
    before do
      allow(ReleaseTools::Prometheus::Query)
        .to receive(:new)
        .and_return(delivery_prometheus)

      allow(delivery_prometheus)
        .to receive_messages(
          monthly_release_status: 3,
          patch_release_status: 1
        )

      allow(ReleaseTools::GitlabReleasesClient)
        .to receive_messages(
          version_for_date: monthly_version,
          current_minor_for_date: current_minor_for_date,
          next_patch_release_date: next_patch_release_date,
          previous_minors: previous_minors
        )

      enable_feature(:release_status_metric_update)
    end

    context 'when updating the metric' do
      it 'creates new monthly and patch release statuses' do
        expect(ReleaseTools::Metrics::MonthlyReleaseStatus).to receive(:new).with(status: :tagged_rc)
          .and_return(instance_double(ReleaseTools::Metrics::MonthlyReleaseStatus, execute: true))

        expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :open)
          .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

        updater.execute
      end

      context 'when feature flag is not set' do
        before do
          disable_feature(:release_status_metric_update)
        end

        it 'does nothing' do
          expect(ReleaseTools::Metrics::MonthlyReleaseStatus)
            .not_to receive(:new)

          expect(ReleaseTools::Metrics::PatchReleaseStatus)
            .not_to receive(:new)

          updater.execute
        end
      end
    end
  end
end
