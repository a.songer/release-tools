# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::Security::AppSecNotifier do
  subject(:notifier) do
    described_class.new(blog_post.web_url, tracking_issue.web_url)
  end

  let(:blog_post) { build(:merge_request) }
  let(:tracking_issue) { build(:issue) }
  let(:local_time) { Time.utc(2023, 5, 14, 15, 55, 0) }

  let(:context_elements) do
    [
      { 'text' => ':clock1: 2023-05-14 15:55 UTC', 'type' => 'mrkdwn' }
    ]
  end

  before do
    allow(ReleaseTools::Slack::Message)
      .to receive(:post)
      .and_return({ 'ok' => true, 'channel' => 'channel' })
  end

  describe '#send_blog_post_notification' do
    it 'posts a message on the AppSec channel' do
      content =
        ":security-tanuki: :ci_passing: *The <#{blog_post.web_url}|blog post> for the <#{tracking_issue.web_url}|upcoming patch release> has been successfully created.*"

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::SEC_APPSEC,
            message: "The patch blog post has been generated: #{blog_post.web_url}",
            blocks: slack_mrkdwn_block(text: content, context_elements: context_elements)
          }
        )

      Timecop.freeze(local_time) do
        notifier.send_blog_post_notification
      end
    end
  end
end
