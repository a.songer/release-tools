# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::Security::TeamNotifier do
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }
  let(:team) { :quality }

  let(:tracking_issue) do
    create(
      :issue,
      due_date: '2022-05-22',
      web_url: 'https://test.com/foo/bar/-/issues/1'
    )
  end

  let(:versions) do
    [
      ReleaseTools::Version.new('16.1.1'),
      ReleaseTools::Version.new('16.0.1'),
      ReleaseTools::Version.new('15.11.1')
    ]
  end

  subject(:team_notifier) { described_class.new(team: team) }

  describe '#notify' do
    before do
      allow(ReleaseTools::Versions)
        .to receive(:next_versions)
        .and_return(versions)

      allow(client)
        .to receive(:next_security_tracking_issue)
        .and_return(tracking_issue)

      allow(ReleaseTools::Slack::Message)
        .to receive(:post)
        .and_return({ 'ok' => true, 'channel' => 'channel' })
    end

    def slack_message
      ":mega: :security-tanuki: Hello, the *<https://test.com/foo/bar/-/issues/1|patch release> has started targeting 2022-05-22 as the due date.* We will be releasing the following packages: *16.1.1, 16.0.1, 15.11.1.* \n A message will be posted to this channel when the patch release is complete. \n For more information about the patch release, check out the 'Release Information' dashboard: https://dashboards.gitlab.net/d/delivery-release_info/delivery3a-release-information?orgId=1"
    end

    it "posts a message to the team's slack channel" do
      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::QUALITY,
            message: instance_of(String),
            blocks: slack_mrkdwn_block(text: slack_message)
          }
        )

      team_notifier.execute
    end

    context 'with another team name' do
      let(:team) { :dedicated }

      it "posts a message to the team's slack channel" do
        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::G_DEDICATED_TEAM,
              message: instance_of(String),
              blocks: slack_mrkdwn_block(text: slack_message)
            }
          )

        team_notifier.execute
      end
    end

    context 'when the team name is invalid' do
      let(:team) { :foo }

      it 'raises an error' do
        expect(ReleaseTools::Slack::Message)
          .not_to receive(:post)

        expect { team_notifier.execute }
          .to raise_error(described_class::CouldNotNotifyError)
      end
    end

    context 'during a critical patch release' do
      before do
        allow(ReleaseTools::SharedStatus)
          .to receive(:critical_security_release?)
          .and_return(true)
      end

      it 'posts a message referencing the release task issue' do
        expect(client)
          .to receive(:current_security_task_issue)
          .and_return(tracking_issue)

        expect(ReleaseTools::Slack::Message)
          .to receive(:post)
          .with(
            {
              channel: ReleaseTools::Slack::QUALITY,
              message: instance_of(String),
              blocks: slack_mrkdwn_block(text: slack_message)
            }
          )

        team_notifier.execute
      end
    end
  end
end
