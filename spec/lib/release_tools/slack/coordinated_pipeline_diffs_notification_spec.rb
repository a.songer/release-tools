# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Slack::CoordinatedPipelineDiffsNotification do
  let(:deploy_version) { '14.3.202108261320-1c87faa073d.2cec58b7eb5' }
  let(:environment) { 'gstg' }
  let(:thread_ts) { 'thread_ts' }
  let(:env_obj) { double(id: 1) }
  let(:presenter) { instance_spy(ReleaseTools::Metadata::Presenter) }

  let(:comparison) do
    instance_spy(
      ReleaseTools::Metadata::Comparison,
      target: ReleaseTools::ProductVersion.new('14.3.202108261121'),
      source: ReleaseTools::ProductVersion.new('14.3.202108261320')
    )
  end

  subject(:notifier) do
    described_class.new(
      deploy_version: deploy_version,
      environment: environment,
      thread_ts: thread_ts
    )
  end

  before do
    compare_service = instance_spy(
      ReleaseTools::Metadata::CompareService,
      with_latest_successful_deployment: comparison
    )
    allow(ReleaseTools::Metadata::CompareService).to receive(:new).and_return(compare_service)

    allow(ReleaseTools::Metadata::Presenter).to receive(:new).and_return(presenter)

    allow(ReleaseTools::GitlabOpsClient)
      .to receive(:environments)
      .with(ReleaseTools::Project::Release::Metadata.ops_path, { name: environment })
      .and_return([env_obj])
  end

  describe '#execute' do
    it 'sends a slack message' do
      header_text = "Diff with 14.3.202108261121 (current version on #{environment})"
      truncated_gitlab_ee_shas = 'gitlab_ee_t...gitlab_ee_s'
      truncated_omnibus_shas = 'omnibus_tar...omnibus_sou'
      trucated_gitaly_sha = 'gitaly_sour'
      gitaly_commit_url = 'https://gitlab.com/gitlab-org/security/gitaly/-/commits/gitaly_source_sha'
      context_text = "<https://ops.gitlab.net/gitlab-org/release/metadata/-/environments/#{env_obj.id}|All #{environment} deployments>"

      allow(presenter)
        .to receive(:component_diffs)
        .and_return([
          "gitlab - <gitlab.com/gitlab-ee|#{truncated_gitlab_ee_shas}>",
          "omnibus-gitlab - <gitlab.com/omnibus|#{truncated_omnibus_shas}>",
          "gitaly - No diff - <#{gitaly_commit_url}|#{trucated_gitaly_sha}> (Last running commit)"
        ])

      block_content = [
        slack_header_block(text: header_text),
        slack_section_block(text: "gitlab - <gitlab.com/gitlab-ee|#{truncated_gitlab_ee_shas}>"),
        slack_section_block(text: "omnibus-gitlab - <gitlab.com/omnibus|#{truncated_omnibus_shas}>"),
        slack_section_block(text: "gitaly - No diff - <#{gitaly_commit_url}|#{trucated_gitaly_sha}> (Last running commit)"),
        slack_divider_block,
        slack_context_block(elements: [{ type: 'mrkdwn', text: context_text }])
      ]

      expect(ReleaseTools::Slack::Message)
        .to receive(:post)
        .with(
          {
            channel: ReleaseTools::Slack::ANNOUNCEMENTS,
            message: header_text,
            blocks: block_content,
            additional_options: {
              thread_ts: thread_ts
            }
          }
        )

      without_dry_run do
        notifier.execute
      end
    end
  end
end
