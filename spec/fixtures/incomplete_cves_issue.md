### Vulnerability Submission

#### Publishing Schedule

After a CVE request is validated, a CVE identifier will be assigned. On what
schedule should the details of the CVE be published? This may be updated at any time and signal when an advisory is ready to publish.

* [ ] Publish immediately
* [x] Wait to publish

#### Request CVE ID
On initial submission, you may automatically request a CVE ID. Be aware, this will not work after the initial submission.

* [x] Automatically assign CVE ID
<!--
Please fill out the yaml codeblock below
-->

```yaml
reporter:
    name: GitLab Security Team
    email: security@example.com
vulnerability:
    description: Vulnerability description
    cwe: CWE-666
    product:
        gitlab_path: gitlab-org/gitlab
        vendor: GitLab
        name: GitLab
        affected_versions:
          - "TODO" # "1.2.3"
          - "TODO" # ">1.3.0, <=1.3.9"
        fixed_versions:
          - "TODO" # "1.2.4"
          - "TODO" # "1.3.10"
    impact: AV:N/AC:L/PR:L/UI:R/S:C/C:L/I:H/A:N
    solution: Upgrade to versions 16.X.X, 16.Y.Y, 16.Z.Z or above.
    credit: Thanks [someone](https://example.com/foo) for reporting this vulnerability through our bug bounty program
    references:
        - https://example.com/bar
```


CVSS scores can be computed by means of the [GitLab CVSS Calculator](https://gitlab-com.gitlab.io/gl-security/appsec/cvss-calculator/) (see also the [CVSS Calculation Guide](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/cvss-calculation.html)).

Alternatively, you can use the [NVD CVSS Calculator](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator).

