//go:build acceptance
// +build acceptance

package main_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

const (
	token = "acceptance tests"
)

var uri string

// TestMain initializes uri according to the testing environment.
// DELIVERY_METRICS_PORT will be set automatically by docker run using --link delivery_metrics
// when not running the tests inside docker, it can be set to just a port number and we will
// connect to http://localhost:port/
func TestMain(m *testing.M) {
	port, hasPort := os.LookupEnv("DELIVERY_METRICS_PORT")

	if !hasPort {
		port = "2112"

		fmt.Println("DELIVERY_METRICS_PORT not set, assuming default port:", port)
	}

	_, portParseErr := strconv.ParseInt(port, 10, 32)
	if portParseErr != nil {
		u, err := url.Parse(port)
		if err != nil {
			fmt.Fprintln(os.Stderr, "Invalid DELIVERY_METRICS_PORT. Use a port number or a URL")
		}

		// we change scheme because docker sets it to tcp
		u.Scheme = "http"
		uri = u.String()
	} else {
		uri = fmt.Sprintf("http://localhost:%v", port)
	}

	fmt.Println("Starting acceptance tests on", uri)

	os.Exit(m.Run())
}

func getMetrics(t *testing.T) string {
	t.Helper()
	resp, err := http.Get(fmt.Sprintf("%v/metrics", uri))
	require.NoErrorf(t, err, "cannot download metrics")

	require.NotNil(t, resp)
	require.NotNil(t, resp.Body)
	body, err := ioutil.ReadAll(resp.Body)
	require.NoErrorf(t, err, "cannot read body")

	return string(body)
}

func getMetric(t *testing.T, metric string) *float64 {
	t.Helper()
	metrics := getMetrics(t)

	r, err := regexp.Compile(fmt.Sprintf("(?m)^%v ([0-9]+(?:\\.[0-9]+)?)$", metric))
	require.NoError(t, err)

	matches := r.FindStringSubmatch(metrics)
	if len(matches) == 0 {
		return nil
	}

	value, err := strconv.ParseFloat(matches[1], 64)
	require.NoError(t, err)

	return &value
}

func webhookRequest(t *testing.T, project string, token string) *http.Request {
	t.Helper()

	jsonData := map[string]interface{}{
		"object_kind":           "build",
		"ref":                   "main",
		"tag":                   false,
		"before_sha":            "0000000000000000000000000000000000000000",
		"sha":                   "95d49d1efbd941908580e79d65e4b5ecaf4a8305",
		"build_id":              3160521140,
		"build_name":            "auto_deploy:metrics:end_time",
		"build_stage":           "coordinated:finish",
		"build_status":          "canceled",
		"build_created_at":      "2022-10-12 08:07:05 UTC",
		"build_started_at":      nil,
		"build_finished_at":     "2022-10-12 08:09:29 UTC",
		"build_duration":        nil,
		"build_queued_duration": nil,
		"build_allow_failure":   true,
		"build_failure_reason":  "unknown_failure",
		"pipeline_id":           664563966,
		"runner":                nil,
		"project_id":            31537070,
		"project_name":          "Reuben Pereira / release-tools-fake",
		"user": map[string]interface{}{
			"id":         2967854,
			"name":       "Reuben Pereira",
			"username":   "rpereira2",
			"avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/2967854/avatar.png",
			"email":      "testuser@acme.com",
		},
		"commit": map[string]interface{}{
			"id":           664563966,
			"sha":          "95d49d1efbd941908580e79d65e4b5ecaf4a8305",
			"message":      "Remove test jobs and add back other jobs",
			"author_name":  "Reuben Pereira",
			"author_email": "test@acme.com",
			"author_url":   "https://gitlab.com/rpereira2",
			"status":       "canceled",
			"duration":     128,
			"started_at":   "2022-10-12 08:07:06 UTC",
			"finished_at":  "2022-10-12 08:09:29 UTC",
		},
		"repository": map[string]interface{}{
			"name":             "release-tools-fake",
			"url":              "git@gitlab.com:rpereira2/release-tools-fake.git",
			"description":      "",
			"homepage":         "https://ops.gitlab.net/gitlab-org/release/tools",
			"git_http_url":     "https://gitlab.com/rpereira2/release-tools-fake.git",
			"git_ssh_url":      "git@gitlab.com:rpereira2/release-tools-fake.git",
			"visibility_level": 20,
		},
		"environment": nil,
	}

	marshalledJson, err := json.Marshal(jsonData)
	require.NoErrorf(t, err, "cannot marshal json data")

	req, err := http.NewRequest("POST", fmt.Sprintf("%v/webhooks/job/%s", uri, project), bytes.NewReader(marshalledJson))
	require.NoErrorf(t, err, "cannot init a job webhook request")

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Gitlab-Token", token)
	req.Header.Set("X-Gitlab-Event", "Job Hook")

	return req
}

func apiRequest(t *testing.T, metric, action, labels, value string) *http.Request {
	t.Helper()

	params := url.Values{}
	if value != "" {
		params.Set("value", value)
	}

	if labels != "" {
		params.Set("labels", labels)
	}

	req, err := http.NewRequest(
		"POST",
		fmt.Sprintf("%v/api/%v/%v", uri, metric, action),
		strings.NewReader(params.Encode()))
	require.NoErrorf(t, err, "cannot init an API request")

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-Private-Token", token)

	return req
}

func TestMetricsRetrieval(t *testing.T) {
	body := getMetrics(t)

	require.Contains(t, body, "delivery_version_info")
}

func TestAPINoToken(t *testing.T) {
	resp, err := http.PostForm(fmt.Sprintf("%v/api/packages_tagging_total/inc", uri), nil)
	require.NoError(t, err)

	require.Equalf(t, http.StatusUnauthorized, resp.StatusCode, "an API request without token must be unauthorized")
}

func TestAPIWrongToken(t *testing.T) {
	req := apiRequest(t, "packages_tagging_total", "inc", "auto_deploy", "")
	req.Header.Set("X-Private-Token", "not a valid token")

	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err, "request failed")

	require.Equalf(t, http.StatusUnauthorized, resp.StatusCode, "an API request without token must be unauthorized")
}

func TestAPIInc(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		expected_metric string
	}{
		{
			metric:          "packages_tagging_total",
			labels:          "auto_deploy,no",
			expected_metric: `delivery_packages_tagging_total{pkg_type="auto_deploy",security="no"}`,
		},
		{
			metric:          "packages_tagging_total",
			labels:          "patch,regular",
			expected_metric: `delivery_packages_tagging_total{pkg_type="patch",security="regular"}`,
		},
		{
			metric:          "packages_tagging_total",
			labels:          "patch,critical",
			expected_metric: `delivery_packages_tagging_total{pkg_type="patch",security="critical"}`,
		},
		{
			metric:          "deployment_duration_last_seconds",
			labels:          "coordinator_pipeline,success,15.10.202303161320-c9acd9d1153.47d29331baf",
			expected_metric: `delivery_deployment_duration_last_seconds{deploy_version="15.10.202303161320-c9acd9d1153.47d29331baf",deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_started_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_started_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "deployment_completed_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_completed_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "deployment_can_rollback_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_can_rollback_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "deployment_rollbacks_started_total",
			labels:          "gstg-cny",
			expected_metric: `delivery_deployment_rollbacks_started_total{target_env="gstg-cny"}`,
		},
		{
			metric:          "auto_deploy_pressure",
			labels:          "gstg",
			expected_metric: `delivery_auto_deploy_pressure{role="gstg"}`,
		},
		{
			metric:          "auto_deploy_picks_total",
			labels:          "success",
			expected_metric: `delivery_auto_deploy_picks_total{status="success"}`,
		},
		{
			metric:          "auto_deploy_picks_total",
			labels:          "failed",
			expected_metric: `delivery_auto_deploy_picks_total{status="failed"}`,
		},
		{
			metric:          "release_pressure",
			labels:          "severity::1,15.5",
			expected_metric: `delivery_release_pressure{severity="severity::1",version="15.5"}`,
		},
		{
			metric:          "auto_deploy_gitlab_pipeline_total",
			labels:          "success",
			expected_metric: `delivery_auto_deploy_gitlab_pipeline_total{status="success"}`,
		},
		{
			metric:          "auto_deploy_gitlab_pipeline_total",
			labels:          "failed",
			expected_metric: `delivery_auto_deploy_gitlab_pipeline_total{status="failed"}`,
		},
		{
			metric:          "release_monthly_status",
			labels:          "2024-02-15,16.9",
			expected_metric: `delivery_release_monthly_status{release_date="2024-02-15",version="16.9"}`,
		},
		{
			metric:          "release_patch_status",
			labels:          "2024-02-15,16.9 16.8 16.7",
			expected_metric: `delivery_release_patch_status{release_date="2024-02-15",versions="16.9 16.8 16.7"}`,
		},
		{
			metric:          "deployment_blocker_count",
			labels:          `RootCause::Flaky-Test,2024-02-15`,
			expected_metric: `delivery_deployment_blocker_count{root_cause="RootCause::Flaky-Test",week="2024-02-15"}`,
		},
		{
			metric:          "deployment_hours_blocked",
			labels:          `RootCause::Flaky-Test,gprd,2024-02-15`,
			expected_metric: `delivery_deployment_hours_blocked{root_cause="RootCause::Flaky-Test",target_env="gprd",week="2024-02-15"}`,
		},
		{
			metric:          "deployment_hours_blocked",
			labels:          `RootCause::Flaky-Test,gstg,2024-02-15`,
			expected_metric: `delivery_deployment_hours_blocked{root_cause="RootCause::Flaky-Test",target_env="gstg",week="2024-02-15"}`,
		},
	}

	for _, example := range examples {
		t.Run(example.metric, func(tt *testing.T) {
			originalValue := getMetric(tt, example.expected_metric)
			if originalValue == nil {
				originalValue = new(float64)
				*originalValue = 0
			}

			req := apiRequest(tt, example.metric, "inc", example.labels, "")

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalValue := getMetric(tt, example.expected_metric)
			require.NotNilf(tt, finalValue, "metric %q not found", example.expected_metric)
			require.Equal(tt, *originalValue+1, *finalValue)
		})
	}
}

func TestAPISet(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		value           float64
		expected_metric string
	}{
		{
			metric:          "deployment_duration_last_seconds",
			labels:          "coordinator_pipeline,success,15.10.202303161320-c9acd9d1153.47d29331baf",
			value:           11.7,
			expected_metric: `delivery_deployment_duration_last_seconds{deploy_version="15.10.202303161320-c9acd9d1153.47d29331baf",deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_started",
			labels:          "gstg-cny,15.10.202303151520-5940b9a5df2.96d2435c16a",
			value:           1,
			expected_metric: `delivery_deployment_started{target_env="gstg-cny",version="15.10.202303151520-5940b9a5df2.96d2435c16a"}`,
		},
		{
			metric:          "deployment_completed",
			labels:          "gstg-cny,15.10.202303151520-5940b9a5df2.96d2435c16a",
			value:           1,
			expected_metric: `delivery_deployment_completed{target_env="gstg-cny",version="15.10.202303151520-5940b9a5df2.96d2435c16a"}`,
		},
		{
			metric:          "deployment_failed_atleast_once",
			labels:          "15.10.202303151520-5940b9a5df2.96d2435c16a",
			value:           1,
			expected_metric: `delivery_deployment_failed_atleast_once{deploy_version="15.10.202303151520-5940b9a5df2.96d2435c16a"}`,
		},
		{
			metric:          "auto_deploy_pressure",
			labels:          "gprd",
			value:           42,
			expected_metric: `delivery_auto_deploy_pressure{role="gprd"}`,
		},
		{
			metric:          "release_pressure",
			labels:          "severity::1,15.5",
			value:           3,
			expected_metric: `delivery_release_pressure{severity="severity::1",version="15.5"}`,
		},
		{
			metric:          "deployment_merge_request_lead_time_seconds",
			labels:          "gprd,main,668398,113498,15.10.202303060320-d244fd30a63.41707614427",
			value:           3,
			expected_metric: `delivery_deployment_merge_request_lead_time_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",deployment_id="668398",mr_id="113498",target_env="gprd",target_stage="main"}`,
		},
		{
			metric:          "deployment_merge_request_adjusted_lead_time_seconds",
			labels:          "gprd,main,668398,113498,15.10.202303060320-d244fd30a63.41707614427",
			value:           3,
			expected_metric: `delivery_deployment_merge_request_adjusted_lead_time_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",deployment_id="668398",mr_id="113498",target_env="gprd",target_stage="main"}`,
		},
		{
			metric:          "deployment_pipeline_duration_seconds",
			labels:          "gitlab-com/gl-infra/deployer,15.10.202303060320-d244fd30a63.41707614427,success,test-pipeline,12345,https://ops.gitlab.net/gitlab-com/gl-infra/production/-/pipelines/12345,,,",
			value:           20,
			expected_metric: `delivery_deployment_pipeline_duration_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",pipeline_id="12345",pipeline_name="test-pipeline",pipeline_status="success",project_name="gitlab-com/gl-infra/deployer",target_env="",target_stage="",upstream_pipeline_name="",web_url="https://ops.gitlab.net/gitlab-com/gl-infra/production/-/pipelines/12345"}`,
		},
		{
			metric:          "deployment_pipeline_duration_seconds",
			labels:          "gitlab-com/gl-infra/deployer,15.10.202303060320-d244fd30a63.41707614427,success,test-pipeline,12345,https://ops.gitlab.net/gitlab-com/gl-infra/production/-/pipelines/12345,gprd,cny,Coordinator pipeline",
			value:           20,
			expected_metric: `delivery_deployment_pipeline_duration_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",pipeline_id="12345",pipeline_name="test-pipeline",pipeline_status="success",project_name="gitlab-com/gl-infra/deployer",target_env="gprd",target_stage="cny",upstream_pipeline_name="Coordinator pipeline",web_url="https://ops.gitlab.net/gitlab-com/gl-infra/production/-/pipelines/12345"}`,
		},
		{
			metric:          "deployment_job_duration_seconds",
			labels:          "job1,test-stage,success,gitlab-com/gl-infra/deployer,15.10.202303060320-d244fd30a63.41707614427,,,job1,https://ops.gitlab.net/gitlab-com/gl-infra/production/-/jobs/2,2,5,Coordinated pipeline",
			value:           20,
			expected_metric: `delivery_deployment_job_duration_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",job_id="2",job_name="job1",job_stage="test-stage",job_status="success",pipeline_id="5",pipeline_name="Coordinated pipeline",project_name="gitlab-com/gl-infra/deployer",short_job_name="job1",target_env="",target_stage="",web_url="https://ops.gitlab.net/gitlab-com/gl-infra/production/-/jobs/2"}`,
		},
		{
			metric:          "deployment_job_duration_seconds",
			labels:          "job1,test-stage,success,gitlab-com/gl-infra/deployer,15.10.202303060320-d244fd30a63.41707614427,gstg,main,job1,https://ops.gitlab.net/gitlab-com/gl-infra/production/-/jobs/2,2,5,Coordinated pipeline",
			value:           20,
			expected_metric: `delivery_deployment_job_duration_seconds{deploy_version="15.10.202303060320-d244fd30a63.41707614427",job_id="2",job_name="job1",job_stage="test-stage",job_status="success",pipeline_id="5",pipeline_name="Coordinated pipeline",project_name="gitlab-com/gl-infra/deployer",short_job_name="job1",target_env="gstg",target_stage="main",web_url="https://ops.gitlab.net/gitlab-com/gl-infra/production/-/jobs/2"}`,
		},
		{
			metric:          "release_monthly_status",
			labels:          "2024-02-15,16.9",
			value:           1,
			expected_metric: `delivery_release_monthly_status{release_date="2024-02-15",version="16.9"}`,
		},
		{
			metric:          "release_patch_status",
			labels:          "2024-02-15,16.9 16.8 16.7",
			value:           1,
			expected_metric: `delivery_release_patch_status{release_date="2024-02-15",versions="16.9 16.8 16.7"}`,
		},
		{
			metric:          "auto_deploy_package_state",
			labels:          "gitlab/omnibus-gitlab,building,15.10.202303060320-d244fd30a63.41707614427",
			value:           1,
			expected_metric: `delivery_auto_deploy_package_state{pkg_state="building",project_path="gitlab/omnibus-gitlab",version="15.10.202303060320-d244fd30a63.41707614427"}`,
		},
		{
			metric:          "auto_deploy_environment_state",
			labels:          "ready,gstg,cny",
			value:           1,
			expected_metric: `delivery_auto_deploy_environment_state{env_state="ready",target_env="gstg",target_stage="cny"}`,
		},
		{
			metric:          "auto_deploy_lock_state",
			labels:          "locked_deployment,gstg,cny",
			value:           1,
			expected_metric: `delivery_auto_deploy_lock_state{lock_reason="locked_deployment",target_env="gstg",target_stage="cny"}`,
		},
		{
			metric:          "deployment_blocker_count",
			labels:          `RootCause::Flaky-Test,2024-02-15`,
			value:           4,
			expected_metric: `delivery_deployment_blocker_count{root_cause="RootCause::Flaky-Test",week="2024-02-15"}`,
		},
		{
			metric:          "deployment_hours_blocked",
			labels:          `RootCause::Flaky-Test,gprd,2024-02-15`,
			value:           1.5,
			expected_metric: `delivery_deployment_hours_blocked{root_cause="RootCause::Flaky-Test",target_env="gprd",week="2024-02-15"}`,
		},
		{
			metric:          "deployment_hours_blocked",
			labels:          `RootCause::Flaky-Test,gstg,2024-02-15`,
			value:           3.0,
			expected_metric: `delivery_deployment_hours_blocked{root_cause="RootCause::Flaky-Test",target_env="gstg",week="2024-02-15"}`,
		},
	}

	for _, example := range examples {
		t.Run(example.metric, func(tt *testing.T) {
			originalValue := getMetric(tt, example.expected_metric)
			if originalValue == nil {
				originalValue = new(float64)
				*originalValue = 0
			}

			req := apiRequest(tt, example.metric, "set", example.labels,
				strconv.FormatFloat(example.value, 'f', -1, 64))

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalValue := getMetric(tt, example.expected_metric)
			require.NotNilf(tt, finalValue, "metric %q not found", example.expected_metric)
			require.Equal(tt, example.value, *finalValue)
		})
	}
}

func TestAPIObserve(t *testing.T) {
	examples := []struct {
		metric          string
		labels          string
		value           float64
		expected_metric string
		expected_bucket float64
	}{
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,success",
			value:           100,
			expected_bucket: 12600,
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,failed",
			value:           35000,
			expected_bucket: 36000,
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			labels:          "coordinator_pipeline,success",
			value:           36550,
			expected_bucket: math.Inf(0),
			expected_metric: `delivery_deployment_duration_seconds_bucket{deployment_type="coordinator_pipeline",status="success"}`,
		},
	}

	for _, example := range examples {
		t.Run(fmt.Sprintf("%v/%v", example.metric, example.value), func(tt *testing.T) {
			countMetric := strings.Replace(example.expected_metric, "_bucket{", "_count{", 1)
			sumMetric := strings.Replace(example.expected_metric, "_bucket{", "_sum{", 1)
			bucketMetric := strings.Replace(example.expected_metric, "}", fmt.Sprintf(`,le="%v"}`, example.expected_bucket), 1)
			bucketMetric = strings.Replace(bucketMetric, "+Inf", "\\+Inf", 1)

			originalCount := getMetric(tt, countMetric)
			if originalCount == nil {
				originalCount = new(float64)
				*originalCount = 0
			}

			originalSum := getMetric(tt, sumMetric)
			if originalSum == nil {
				originalSum = new(float64)
				*originalSum = 0
			}

			originalValue := getMetric(tt, bucketMetric)
			if originalValue == nil {
				originalValue = new(float64)
				*originalValue = 0
			}

			req := apiRequest(tt, example.metric, "observe", example.labels,
				strconv.FormatFloat(example.value, 'f', -1, 64))

			resp, err := http.DefaultClient.Do(req)
			require.NoError(tt, err, "request failed")

			body, _ := ioutil.ReadAll(resp.Body)
			require.Equal(tt, http.StatusOK, resp.StatusCode, string(body))

			finalCount := getMetric(tt, countMetric)
			require.NotNil(tt, finalCount)
			require.Equal(tt, *originalCount+1, *finalCount)
			finalSum := getMetric(tt, sumMetric)
			require.NotNil(tt, finalSum)
			require.Equal(tt, *originalSum+example.value, *finalSum)
			finalValue := getMetric(tt, bucketMetric)
			require.NotNil(tt, finalValue)
			require.Equal(tt, *originalValue+1, *finalValue)
		})
	}
}

func TestAPIDelete(t *testing.T) {
	dataset := []struct {
		metric          string
		request         string
		value           string
		labels          string
		expected_metric string
	}{
		{
			metric:          "packages_tagging_total",
			request:         "inc",
			labels:          "auto_deploy,no",
			expected_metric: `delivery_packages_tagging_total{pkg_type="auto_deploy",security="no"}`,
		},
		{
			metric:          "deployment_duration_last_seconds",
			request:         "set",
			value:           "10",
			labels:          "coordinator_pipeline,success,15.10.202303161320-c9acd9d1153.47d29331baf",
			expected_metric: `delivery_deployment_duration_last_seconds{deploy_version="15.10.202303161320-c9acd9d1153.47d29331baf",deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_last_seconds",
			request:         "set",
			value:           "13",
			labels:          "coordinator_pipeline,failed,15.10.202303161320-c9acd9d1153.47d29331baf",
			expected_metric: `delivery_deployment_duration_last_seconds{deploy_version="15.10.202303161320-c9acd9d1153.47d29331baf",deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,success",
			value:           "100",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="success"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,failed",
			value:           "35000",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="failed"}`,
		},
		{
			metric:          "deployment_duration_seconds",
			request:         "observe",
			labels:          "coordinator_pipeline,success",
			value:           "36550",
			expected_metric: `delivery_deployment_duration_seconds_count{deployment_type="coordinator_pipeline",status="success"}`,
		},
	}

	// let's populate metrics with each type of metric we have and multiple label values
	for _, data := range dataset {
		req := apiRequest(t, data.metric, data.request, data.labels, data.value)

		resp, err := http.DefaultClient.Do(req)
		require.NoError(t, err, "request failed")

		body, _ := ioutil.ReadAll(resp.Body)
		require.Equal(t, http.StatusOK, resp.StatusCode, string(body))

		metricValue := getMetric(t, data.expected_metric)
		require.NotNilf(t, metricValue, "metric %q not found", data.expected_metric)
	}

	// reset all the metrics
	for _, data := range dataset {
		req, err := http.NewRequest(
			"DELETE",
			fmt.Sprintf("%v/api/%v", uri, data.metric),
			nil)
		require.NoErrorf(t, err, "cannot init the reset request")

		req.Header.Set("X-Private-Token", token)

		resp, err := http.DefaultClient.Do(req)
		require.NoError(t, err, "reset request failed")

		body, _ := ioutil.ReadAll(resp.Body)
		require.Equal(t, http.StatusOK, resp.StatusCode, string(body))

		metricValue := getMetric(t, data.expected_metric)
		require.Nilf(t, metricValue, "metric %q was not reset", data.expected_metric)
	}
}

func TestReleaseToolsJobWebhookWrongToken(t *testing.T) {
	req := webhookRequest(t, "ops/gitlab-org/release/tools", "wrong-token")

	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err, "request failed")

	require.Equalf(t, http.StatusUnauthorized, resp.StatusCode, "an API request without token must be unauthorized")
}

func TestReleaseToolsJobWebhookCorrectToken(t *testing.T) {
	req := webhookRequest(t, "ops/gitlab-org/release/tools", "release-tools-token")

	resp, err := http.DefaultClient.Do(req)
	require.NoError(t, err, "request failed")

	respErr, err := io.ReadAll(resp.Body)
	require.NoError(t, err)
	require.Equalf(t, http.StatusOK, resp.StatusCode, "an API request with token must be successful; ", string(respErr))
}
