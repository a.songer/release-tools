package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
)

func PrivateTokenMiddleware(authToken string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := r.Header.Get("X-Private-Token")
			if token != authToken {
				w.WriteHeader(http.StatusUnauthorized)
				answer(w, r, "Missing or wrong X-Private-Token")

				return
			}

			next.ServeHTTP(w, r)
		})
	}
}
