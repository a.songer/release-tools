package labels

const (
	SUCCESS  = "success"
	FAILED   = "failed"
	MANUAL   = "manual"
	CANCELED = "canceled"
	SKIPPED  = "skipped"
	RUNNING  = "running"

	ProjectDeployer                = "gitlab-com/gl-infra/deployer"
	ProjectReleaseTools            = "gitlab-org/release/tools"
	ProjectQualityStagingCanary    = "gitlab-org/quality/staging-canary"
	ProjectQualityStaging          = "gitlab-org/quality/staging"
	ProjectQualityStagingRef       = "gitlab-org/quality/staging-ref"
	ProjectQualityProductionCanary = "gitlab-org/quality/canary"
	ProjectQualityProduction       = "gitlab-org/quality/production"
	ProjectK8sWorkloadsGitlabCom   = "gitlab-com/gl-infra/k8s-workloads/gitlab-com"
	ProjectCng                     = "gitlab/charts/components/images"
	ProjectOmnibus                 = "gitlab/omnibus-gitlab"
)

func FromValues(name string, values []string) *stringSlice {
	return &stringSlice{
		base:   &base{name: name},
		values: values,
	}
}

func PipelineStatus(name string) *stringSlice {
	return FromValues(name, []string{SUCCESS, FAILED, MANUAL, CANCELED, SKIPPED, RUNNING})
}

func SuccessOrFailed(name string) *stringSlice {
	return FromValues(name, []string{SUCCESS, FAILED})
}

func AutoDeployProjects(name string) *stringSlice {
	return FromValues(name, []string{
		ProjectDeployer,
		ProjectReleaseTools,
		ProjectQualityStagingCanary,
		ProjectQualityStaging,
		ProjectQualityStagingRef,
		ProjectQualityProductionCanary,
		ProjectQualityProduction,
		ProjectK8sWorkloadsGitlabCom,
		ProjectCng,
		ProjectOmnibus,
	})
}

func PackagerProjects(name string) *stringSlice {
	return FromValues(name, []string{
		ProjectOmnibus,
		ProjectCng,
	})
}

func Environment(env string) *stringSlice {
	return FromValues(env, []string{"gstg", "gprd", "pre", "gstg-ref", "ops", "testbed", "db-benchmarking"})
}

func Stage(env string) *stringSlice {
	return FromValues(env, []string{"main", "cny"})
}

type stringSlice struct {
	values []string

	*base
}

func (s *stringSlice) Values() []string {
	return s.values
}

func (s *stringSlice) CheckValue(value string) bool {
	for _, allowedValue := range s.values {
		if value == allowedValue {
			return true
		}
	}

	return false
}
