# Jobs associated to https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/154

.if-coordinated-pipeline: &if-coordinated-pipeline
  if: '$CI_COMMIT_TAG =~ /^\d+\.\d+\.\d{12}$/'

.if-auto-deploy-tag: &if-auto-deploy-tag
  if: '$AUTO_DEPLOY_TAG'

.unless-skip-staging-ref: &unless-skip-staging-ref
  if:  "$SKIP_REF_CNY_DEPLOYMENT && $DEPLOY_ENVIRONMENT == 'gstg-ref'"
  when: never

.unless-skip-gstg-cny: &unless-skip-gstg-cny
  if:  "$SKIP_GSTG_CNY_DEPLOYMENT && $DEPLOY_ENVIRONMENT == 'gstg-cny'"
  when: never

.track_deployment_started:
  extends: .with-bundle
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake 'metrics:deployment_started'

.track_deployment_completed:
  extends: .with-bundle
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  allow_failure: true
  script:
    - bundle exec rake 'metrics:deployment_completed'

.notify_base:
  extends: .with-bundle
  variables:
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
  allow_failure: true
  script:
    - bundle exec rake 'auto_deploy:notify'

.notify_start:
  extends: .notify_base
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success

.notify_success:
  extends: .notify_base
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success

.notify_failure:
  extends: .notify_base
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure

.environment-state-metric:
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  tags:
    - release
  allow_failure: true
  resource_group: set-environment-state-metric:$DEPLOY_ENVIRONMENT

.environment-lock-state-metric:
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  tags:
    - release
  allow_failure: true
  resource_group: set-environment-locked-state-metric:$DEPLOY_ENVIRONMENT


# Create package tags in order to initiate the building process.
#
# This job will only run on a tagged release-tools pipeline matching a specific
# format.
auto_deploy:start:
  extends: .with-bundle
  stage: coordinated:tag
  # this job writes to release/metadata, it cannot run in cuncurrently
  resource_group: release-metadata
  rules:
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake 'auto_deploy:start'
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Wait on an Omnibus pipeline to complete in order to ensure the package is
# built before attempting to deploy it.
#
# Runs under one of two conditions:
#
# 1. In a tagged release-tools pipeline after `auto_deploy:start`, delayed by 60
#    minutes since packages usually take at least this long to build.
# 2. In the presence of an `AUTO_DEPLOY_TAG` variable, populated when we want to
#    trigger a deploy directly from ChatOps. There is no delay in this case.
wait:omnibus:
  extends: .with-bundle
  stage: coordinated:build
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 45 minutes
  script:
    - bundle exec rake 'auto_deploy:wait:omnibus'

# Wait on a CNG pipeline to complete in order to ensure the image is
# built before attempting to deploy it.
#
# Runs under one of two conditions:
#
# 1. In a tagged release-tools pipeline after `auto_deploy:start`, delayed by 45
#    minutes since images usually take at least this long to build.
# 2. In the presence of an `AUTO_DEPLOY_TAG` variable, populated when we want to
#    trigger a deploy directly from ChatOps. There is no delay in this case.
wait:cng:
  extends: .with-bundle
  stage: coordinated:build
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 60 minutes
  script:
    - bundle exec rake 'auto_deploy:wait:cng'

metrics:package_state:start:
  extends: .with-bundle
  stage: coordinated:build
  allow_failure: true
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 3 minutes
  needs:
    - auto_deploy:start
  script:
    - bundle exec rake 'metrics:package_state'

metrics:package_state:failed:
  extends: .with-bundle
  stage: coordinated:build
  allow_failure: true
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: on_failure
  needs:
    - auto_deploy:start
    - wait:cng
    - wait:omnibus
  script:
    - bundle exec rake 'metrics:package_state'

metrics:package_state:success:
  extends: .with-bundle
  stage: coordinated:build
  allow_failure: true
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
      when: on_success
  needs:
    - auto_deploy:start
    - wait:cng
    - wait:omnibus
  script:
    - bundle exec rake 'metrics:package_state'

# Registers the deployment start time and stores the value in `DEPLOY_START_TIME`.
#
# Runs after the auto_deploy packages have been built and before the gstg-cny
# deployment starts.
prepare:
  extends: .with-bundle
  stage: coordinated:prepare
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:cng
  script:
    - bundle exec rake 'auto_deploy:metrics:start_time'
  allow_failure: true
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Validates if the built package has a green pipeline on gitlab security
validate pipeline:
  extends: .with-bundle
  stage: coordinated:prepare
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:cng
  script:
    - bundle exec rake 'auto_deploy:validate_pipeline'

# Creates our QA tracking issue
# https://gitlab.com/gitlab-org/release/tasks/-/issues?sort=milestone_due_desc&state=opened&label_name[]=QA+task
create-qa-issue:
  extends: .with-bundle
  stage: coordinated:prepare
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  allow_failure: true
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:cng
  script:
    - bundle exec rake "release:create_qa_issue[$DEPLOY_VERSION]"

# Send a slack notification about the start of the staging ref deployment.
#
# Runs after the initial metrics (metrics:start) have been collected.
notify_start:gstg-ref:
  extends: .notify_start
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - prepare
    - validate pipeline

# Trigger a downstream pipeline to deploy to staging ref
deploy:gstg-ref:
  stage: coordinated:deploy:staging-canary-and-ref
  allow_failure: true
  rules:
    - <<: *unless-skip-staging-ref
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:cng
    - prepare
    - validate pipeline
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

# metrics:deployment_started:gstg-ref:
#   extends: .track_deployment_started
#   stage: coordinated:deploy:staging-canary-and-ref
#   variables:
#     DEPLOY_ENVIRONMENT: 'gstg-ref'
#   needs:
#     - auto_deploy:start # we need DEPLOY_VERSION env artifact
#     - prepare

# Sends a slack notification notifying a successful deployment for gstg-ref
notify_success:gstg-ref:
  extends: .notify_success
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - deploy:gstg-ref

# Sends a slack notification notifying a failed deployment for gstg-ref
notify_failure:gstg-ref:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-ref'
  needs:
    - auto_deploy:start
    - deploy:gstg-ref

# Send a slack notification about the start of the staging canary deployment.
#
# Runs after the initial metrics (metrics:start) have been collected.
notify_start:gstg-cny:
  extends: .notify_start
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - prepare
    - validate pipeline

# Trigger a downstream pipeline to deploy to staging canary
deploy:gstg-cny:
  stage: coordinated:deploy:staging-canary-and-ref
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - auto_deploy:start
    - wait:omnibus
    - wait:cng
    - prepare
    - validate pipeline
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

metrics:deployment_started:gstg-cny:
  extends: .track_deployment_started
  stage: coordinated:deploy:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - prepare
    - validate pipeline

metrics:deployment_completed:gstg-cny:
  extends: .track_deployment_completed
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'

metrics:environment-lock-state:qa:gstg-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:qa:staging-canary
  variables:
    DEPLOY_ENVIRONMENT: gstg-cny
  needs:
    - deploy:gstg-cny
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_qa]"

qa:smoke:gstg-cny:
  stage: coordinated:qa:staging-canary
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gstg-cny
  trigger:
    project: gitlab-org/quality/staging-canary
    strategy: depend

qa:smoke-main:gstg-cny:
  stage: coordinated:qa:staging-canary
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gstg-cny
  trigger:
    project: gitlab-org/quality/staging
    strategy: depend

# Triggers a Full Quality pipeline on quality/staging-canary
qa:full:gstg-cny:
  stage: coordinated:qa:staging-canary
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    FULL_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gstg-cny
  trigger:
    project: gitlab-org/quality/staging-canary

validate_ownership:gstg-cny:
  extends: .pipeline-ownership-check
  stage: coordinated:finish:staging-canary-and-ref
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny

# Updates pending QA reports on gstg-cny
update_qa_report:gstg-cny:
  extends: .with-bundle
  stage: coordinated:finish:staging-canary-and-ref
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  allow_failure: true
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny
  script:
    - bundle exec rake "quality:update_report[$DEPLOY_VERSION, $DEPLOY_ENVIRONMENT]"

# Sends a slack notification notifying a successful deployment for gstg-cny
notify_success:gstg-cny:
  extends: .notify_success
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny

# Sends a slack notification notifying a failed deployment for gstg-cny
notify_failure:gstg-cny:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - deploy:gstg-cny

metrics:environment-lock-state:failure:gstg-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_deployment_failed]"
  needs:
    - deploy:gstg-cny

notify_failure:gstg-cny:qa:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny
  script:
    - bundle exec rake 'quality:notify'

metrics:environment-lock-state:failure:gstg-cny:qa:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: gstg-cny
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_qa_failed]"
  needs:
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny

metrics:environment-state:gstg-cny:qa_success:
  extends: .environment-state-metric
  stage: coordinated:finish:staging-canary-and-ref
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny
  script:
    - bundle exec rake "metrics:environment_state[gstg-cny, ready]"

metrics:environment-lock-state:success:gstg-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: gstg-cny
  rules:
    - <<: *unless-skip-gstg-cny
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    # Nil lock_reason
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT,]"
  needs:
    - qa:smoke-main:gstg-cny
    - qa:smoke:gstg-cny

report_failure:gstg-cny:qa:
  extends: .notify_failure
  stage: coordinated:finish:staging-canary-and-ref
  variables:
    DEPLOY_ENVIRONMENT: 'gstg-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gstg-cny
    - qa:smoke-main:gstg-cny
  script:
    - bundle exec rake 'quality:report_failure'

# Send a slack notification about the start of the canary deployment
#
# Runs after the notification of the staging canary deployment has been sent.
notify_start:gprd-cny:
  extends: .notify_start
  stage: coordinated:deploy:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - job: auto_deploy:start
    - job: notify_success:gstg-cny
      optional: true
    - job: validate_ownership:gstg-cny
      optional: true

# Trigger a downstream pipeline to deploy to canary
deploy:gprd-cny:
  stage: coordinated:deploy:canary
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
  needs:
    - job: auto_deploy:start
    - job: prepare
    - job: notify_success:gstg-cny
      optional: true
    - job: validate_ownership:gstg-cny
      optional: true
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

deploy:ring0:
  stage: coordinated:deploy:canary
  rules:
    - if: $FF_RING0 == null
      when: never
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_VERSION: $DEPLOY_VERSION
  allow_failure: true
  resource_group: ring_0
  needs:
    - job: auto_deploy:start
    - job: prepare
    - job: notify_success:gstg-cny
      optional: true
    - job: validate_ownership:gstg-cny
      optional: true
  trigger:
    include: .gitlab/ci/ring_0.gitlab-ci.yml
    # TODO: make it dependant
    # strategy: depend

metrics:deployment_started:gprd-cny:
  extends: .track_deployment_started
  stage: coordinated:deploy:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - job: auto_deploy:start # we need DEPLOY_VERSION env artifact
    - job: notify_success:gstg-cny
      optional: true
    - job: validate_ownership:gstg-cny
      optional: true

# Set lock state metric before qa run
metrics:environment-lock-state:qa:gprd-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:qa:canary
  variables:
    DEPLOY_ENVIRONMENT: gprd-cny
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_qa]"
  needs:
    - deploy:gprd-cny

# Triggers a Full Quality pipeline on quality/canary
qa:full:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    FULL_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/canary

# Triggers a Smoke Quality pipeline on quality/canary
qa:smoke:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/canary
    strategy: depend

# Triggers an Smoke Quality pipeline on quality/production
qa:smoke-main:gprd-cny:
  stage: coordinated:qa:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SMOKE_ONLY: 'true'
    GITLAB_QA_ISSUE_URL: ''
  needs:
    - auto_deploy:start
    - deploy:gprd-cny
  trigger:
    project: gitlab-org/quality/production
    strategy: depend

metrics:environment-state:gprd-cny:qa_success:
  extends: .environment-state-metric
  stage: coordinated:finish:canary
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny
  script:
    - bundle exec rake "metrics:environment_state[gprd-cny, baking_time]"

metrics:environment-lock-state:success:gprd-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: gprd-cny
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    # Nil lock_reason
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT,]"
  needs:
    - qa:smoke-main:gprd-cny
    - qa:smoke:gprd-cny

validate_ownership:gprd-cny:
  extends: .pipeline-ownership-check
  stage: coordinated:finish:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  needs:
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny

# Updates pending QA reports on gstg-cny
update_qa_report:gprd-cny:
  extends: .with-bundle
  stage: coordinated:finish:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_success
    - <<: *if-coordinated-pipeline
      when: on_success
  allow_failure: true
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny
  script:
    - bundle exec rake "quality:update_report[$DEPLOY_VERSION, $DEPLOY_ENVIRONMENT]"

# Sends a slack notification notifying a successful deployment for gprd-cny
notify_success:gprd-cny:
  extends: .notify_success
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny

metrics:deployment_completed:gprd-cny:
  extends: .track_deployment_completed
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'

# Sends a slack notification notifying a failed deployment for gprd-cny
notify_failure:gprd-cny:
  extends: .notify_failure
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - deploy:gprd-cny

metrics:environment-lock-state:failure:gprd-cny:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:canary
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  variables:
    DEPLOY_ENVIRONMENT: gprd-cny
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_deployment_failed]"
  needs:
    - deploy:gprd-cny

# Sends a slack notification notifying a failed QA for gprd-cny
notify_failure:gprd-cny:qa:
  extends: .notify_failure
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny
  script:
    - bundle exec rake 'quality:notify'

metrics:environment-lock-state:failure:gprd-cny:qa:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: gprd-cny
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_qa_failed]"
  needs:
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny

# Opens a release/tasks issue for a failed QA for gprd-cny
report_failure:gprd-cny:qa:
  extends: .notify_failure
  stage: coordinated:finish:canary
  variables:
    DEPLOY_ENVIRONMENT: 'gprd-cny'
  needs:
    - auto_deploy:start
    - qa:smoke:gprd-cny
    - qa:smoke-main:gprd-cny
  script:
    - bundle exec rake 'quality:report_failure'

# Triggers production checks before authorizing a deployment to production
# and staging
# Runs after 30 minutes and if a deployment to staging canary
# has been completed.
baking_time:
  tags:
    # Internal prometheus is only available from specific tagged runners
    - release
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  stage: coordinated:promote
  rules:
    - <<: *if-auto-deploy-tag
      when: delayed
      start_in: 30 minutes
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 30 minutes
  needs:
    - notify_success:gprd-cny
  script:
    - export LOG_LEVEL=trace  # More verbose logging while we debug
    - unset ELASTIC_URL       # Don't send these verbose logs to Elastic
    - bundle exec rake 'auto_deploy:baking_time'

metrics:environment-state:awaiting_promotion:
  extends: .environment-state-metric
  stage: coordinated:promote
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - baking_time
  script:
    - bundle exec rake "metrics:environment_state[gprd-cny, ready]"
    - bundle exec rake "metrics:environment_state[gstg, awaiting_promotion]"
    - bundle exec rake "metrics:environment_state[gprd, awaiting_promotion]"

# Triggers a production check that validates if a deploy to production
# can start
promote:
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  tags:
    - release
  stage: coordinated:promote
  rules:
    - <<: *if-auto-deploy-tag
      when: manual
    - <<: *if-coordinated-pipeline
      when: manual
  needs:
    - auto_deploy:start
    - notify_success:gprd-cny
    - validate_ownership:gprd-cny
  script:
    - bundle exec rake 'auto_deploy:promotion_checks'
  artifacts:
    reports:
      dotenv: deploy_vars.env

# Send a slack notification about the start of the staging deployment.
#
# Runs after the notification of the staging-canary deployment has been sent.
notify_start:gstg:
  extends: .notify_start
  stage: coordinated:deploy:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - promote

# Trigger a downstream pipeline to deploy to staging
deploy:gstg:
  stage: coordinated:deploy:staging
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
    SKIP_QA_ISSUE_CREATION: 'true'
    POSTDEPLOY_MIGRATIONS: 'false'
  needs:
    - auto_deploy:start
    - prepare
    - promote
    - validate_ownership:gprd-cny
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

metrics:deployment_started:gstg:
  extends: .track_deployment_started
  stage: coordinated:deploy:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - promote

production:checks:
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  tags:
    - release
  stage: coordinated:promote
  rules:
    - <<: *if-auto-deploy-tag
      when: delayed
      start_in: 30 minutes
    - <<: *if-coordinated-pipeline
      when: delayed
      start_in: 30 minutes
  needs:
    - promote # we need DEPLOY_VERSION and IGNORE_PRODUCTION_CHECKS env artifacts
  script:
    - bundle exec rake 'auto_deploy:gprd_guard'

# Send a slack notification about the start of the production deployment
#
# Runs after the deployment checks (promote:gprd)
# have been cleared
notify_start:gprd:
  extends: .notify_start
  stage: coordinated:deploy:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - production:checks

metrics:deployment_started:gprd:
  extends: .track_deployment_started
  stage: coordinated:deploy:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start # we need DEPLOY_VERSION env artifact
    - production:checks

# Triggers a downstream pipeline to deploy to production
#
# Runs after the deployment checks (promote:gprd)
# have been cleared
deploy:gprd:
  stage: coordinated:deploy:production
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
    TRIGGER_REF: 'master'
    DEPLOY_USER: 'deployer'
    DEPLOY_VERSION: $DEPLOY_VERSION
    SKIP_JOB_ON_COORDINATOR_PIPELINE: 'true'
    SKIP_PIPELINE_NOTIFY: 'true'
    POSTDEPLOY_MIGRATIONS: 'false'
  needs:
    - auto_deploy:start
    - production:checks
    - prepare
    - validate_ownership:gprd-cny
  trigger:
    project: gitlab-com/gl-infra/deployer
    strategy: depend

# Sends a slack notification notifying a successful deployment for gstg
notify_success:gstg:
  extends: .notify_success
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - deploy:gstg

metrics:deployment_completed:gstg:
  extends: .track_deployment_completed
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'

metrics:environment-state:success:gstg:
  extends: .environment-state-metric
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: gstg
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake "metrics:environment_state[$DEPLOY_ENVIRONMENT, ready]"
  needs:
    - deploy:gstg

metrics:environment-lock-state:success:gstg:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: gstg
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    # Nil lock_reason
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT,]"
  needs:
    - deploy:gstg

# Sends a slack notification notifying a failed deployment for gstg
notify_failure:gstg:
  extends: .notify_failure
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: 'gstg'
  needs:
    - auto_deploy:start
    - deploy:gstg

metrics:environment-lock-state:failure:gstg:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:staging
  variables:
    DEPLOY_ENVIRONMENT: gstg
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_deployment_failed]"
  needs:
    - deploy:gstg

# Sends a slack notification notifying a successful deployment for gprd
notify_success:gprd:
  extends: .notify_success
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - deploy:gprd

metrics:deployment_completed:gprd:
  extends: .track_deployment_completed
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'

# Sends a slack notification notifying a failed deployment for gprd
notify_failure:gprd:
  extends: .notify_failure
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: 'gprd'
  needs:
    - auto_deploy:start
    - deploy:gprd

metrics:environment-lock-state:failure:gprd:
  extends: .environment-lock-state-metric
  stage: coordinated:finish:production
  variables:
    DEPLOY_ENVIRONMENT: gprd
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  script:
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT, locked_deployment_failed]"
  needs:
    - deploy:gprd

# Registers the deployment end time and calculates the deployment duration
# based on `DEPLOY_START_TIME` value. Pushes the information to Prometheus PushGateway
#
# Runs after the deployment to production finishes.
metrics:end_time:
  tags:
    # Internal prometheus is only available from specific tagged runners
    - release
  extends:
    - .with-bundle
    - .mimir-query-auth
    - .id-tokens
  stage: coordinated:finish
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  variables:
    DEPLOY_START_TIME: $DEPLOY_START_TIME
    DEPLOY_VERSION: $DEPLOY_VERSION
  allow_failure: true
  needs:
    - auto_deploy:start
    - prepare
    - notify_success:gprd
  script:
    - bundle exec rake 'auto_deploy:metrics:end_time'

metrics:deployment_failed_atleast_once:
  extends: .with-bundle
  stage: coordinated:finish
  rules:
    - <<: *if-auto-deploy-tag
      when: on_failure
    - <<: *if-coordinated-pipeline
      when: on_failure
  allow_failure: true
  variables:
    DEPLOY_VERSION: $DEPLOY_VERSION
  needs:
    - auto_deploy:start
  script:
    - bundle exec rake 'metrics:deployment_failed_atleast_once'

metrics:play-manual-jobs:
  extends: .with-bundle
  stage: coordinated:finish
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake "auto_deploy:metrics:play_manual_jobs[$CI_PIPELINE_ID]"

metrics:trace-pipeline:
  extends: .with-bundle
  stage: coordinated:finish
  retry: 2
  rules:
    - <<: *if-auto-deploy-tag
      when: manual
    - <<: *if-coordinated-pipeline
      when: manual
  needs:
    - auto_deploy:start
  variables:
    GITLAB_O11Y_URL: "https://observe.gitlab.com/v3/9970/430285/ingest/traces"
  script:
    - bundle exec rake "trace:pipeline_url[$CI_PIPELINE_URL,deployment pipelines,$DEPLOY_VERSION]"

metrics:pipeline_and_job_duration:
  extends: .with-bundle
  stage: coordinated:finish
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  needs:
    - auto_deploy:start
    - metrics:trace-pipeline
  script:
    - bundle exec rake "metrics:pipeline_and_job_duration[$CI_PIPELINE_URL,$DEPLOY_VERSION]"
  retry: 2

metrics:environment-state:success:gprd:
  extends: .environment-state-metric
  stage: coordinated:finish
  variables:
    DEPLOY_ENVIRONMENT: gprd
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    - bundle exec rake "metrics:environment_state[$DEPLOY_ENVIRONMENT, ready]"
  needs:
    - deploy:gprd

metrics:environment-lock-state:success:gprd:
  extends: .environment-lock-state-metric
  stage: coordinated:finish
  variables:
    DEPLOY_ENVIRONMENT: gprd
  rules:
    - <<: *if-auto-deploy-tag
    - <<: *if-coordinated-pipeline
  script:
    # Nil lock_reason
    - bundle exec rake "metrics:environment_lock_state[$DEPLOY_ENVIRONMENT,]"
  needs:
    - deploy:gprd
