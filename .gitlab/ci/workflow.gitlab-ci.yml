workflow:
  name: "$PIPELINE_NAME"
  rules:
    # Pipelines defined in .gitlab/ci/chatops.gitlab-ci.yml
    - if: '$TASK'
      variables:
        PIPELINE_NAME: "chatops $TASK"

    # Renovate pipelines
    - if: "$RENOVATE_SCHEDULED || $RENOVATE_IMMEDIATE"
      variables:
        PIPELINE_NAME: "renovate-bot"

    # Pipelines defined in .gitlab/ci/coordinated-pipeline.gitlab-ci.yml
    - if: !reference [.if-auto-deploy-tag, if]
      variables:
        PIPELINE_NAME: "Coordinator pipeline with overridden tag"
    - if: !reference [.if-coordinated-pipeline, if]
      variables:
        PIPELINE_NAME: "Coordinator pipeline"

    # Pipelines defined in .gitlab/ci/post-deploy-migrations-pipeline.gitlab-ci.yml
    - if: !reference [.if-post-deploy-pipeline, if]
      variables:
        PIPELINE_NAME: "Execute post deploy migrations"

    # Pipelines defined in .gitlab/ci/metrics.gitlab-ci.yml
    - if: '$MIMIR_URL && $PUSHGATEWAY_URL && $PUSH_METRICS'
      variables:
        PIPELINE_NAME: "Update metrics"

    # Pipelines defined in .gitlab/ci/mirror-status.gitlab-ci.yml
    - if: $MIRROR_STATUS == 'true'
      variables:
        PIPELINE_NAME: 'Security mirror status'

    # Pipelines defined in .gitlab/ci/auto-deploy.gitlab-ci.yml
    - if: $AUTO_DEPLOY_CLEANUP == "true"
      variables:
        PIPELINE_NAME: "auto_deploy:cleanup"
    - if: $CHECK_PRODUCTION == "true"
      variables:
        PIPELINE_NAME: "auto_deploy:check_production"
    - if: $CHECK_PACKAGE == "true"
      variables:
        PIPELINE_NAME: "auto_deploy:check_package"


    # Pipelines defined in .gitlab/ci/automation.gitlab-ci.yml
    - if: $RELEASE_MANAGERS && $CI_PIPELINE_SOURCE == "schedule"
      variables:
        PIPELINE_NAME: 'Sync release managers'

    - if: "$JOIN_RELEASE_MANAGERS"
      variables:
        PIPELINE_NAME: "Join release managers"

    - if: "$LEAVE_RELEASE_MANAGERS"
      variables:
        PIPELINE_NAME: "Leave release managers"

    - if: $CLOSE_EXPIRED_QA_ISSUES && $CI_PIPELINE_SOURCE == "schedule"
      variables:
        PIPELINE_NAME: "Close expired QA issues"

    - if: $VALIDATE_SECURITY_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "schedule"
      variables:
        PIPELINE_NAME: "Validate security MRs"

    - if: $SECURITY_MERGE_TRAIN == '1'
      variables:
        PIPELINE_NAME: "Toggle canonical to security merge train"

    - if: $CI_PIPELINE_SOURCE != "merge_request_event" &&  $CI_PIPELINE_SOURCE != "push" && $UPDATE_COMPONENTS == "true"
      variables:
        PIPELINE_NAME: "Update Gitaly"

    - if: $TRACK_DEPLOYMENT == "true"
      variables:
        PIPELINE_NAME: "Track deployment $DEPLOY_ENVIRONMENT $DEPLOY_STATUS $DEPLOY_VERSION"

    - if: $RECORD_METADATA_DEPLOYMENT == "true"
      variables:
        PIPELINE_NAME: "Record metadata deployment $DEPLOY_ENVIRONMENT $DEPLOY_STATUS $DEPLOY_VERSION"

    - if: $CREATE_QA_ISSUE == "true"
      variables:
        PIPELINE_NAME: "Create QA issue $DEPLOY_ENVIRONMENT $DEPLOY_STATUS $DEPLOY_VERSION"

    - if: $TAG_SCHEDULED_RC == "true" && $CI_PIPELINE_SOURCE == "schedule"
      variables:
        PIPELINE_NAME: "Tag scheduled RC"

    - if: $BUILD_POST_MIGRATIONS == "true"
      variables:
        # This name should match the PIPELINE_NAME constant in auto_deploy/post_deploy_migrations/pending.rb
        PIPELINE_NAME: "Build post deployment migration artifact"

    - if: $DEPLOYMENT_BLOCKERS_REPORT && $CI_PIPELINE_SOURCE == "schedule"
      variables:
        PIPELINE_NAME: "Generate deployment blockers report"

    - if: $TRACE_PIPELINE
      variables:
        PIPELINE_NAME: "Trace pipeline"

    # Pipelines defined in .gitlab/ci/security/
    - if: $SECURITY_RELEASE_PIPELINE == 'prepare'
      variables:
        PIPELINE_NAME: "Security release prepare pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'process'
      variables:
        PIPELINE_NAME: "Security release process pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'early_merge'
      variables:
        PIPELINE_NAME: "Security release early merge (default branch) pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'backport_merge'
      variables:
        PIPELINE_NAME: "Security release backport merge (stable branches) pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'release_preparation'
      variables:
        PIPELINE_NAME: "Security release release preparation pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'publish'
      variables:
        PIPELINE_NAME: "Security release publish pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'finalize'
      variables:
        PIPELINE_NAME: "Security release finalize pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'tag'
      variables:
        PIPELINE_NAME: "Security tagging pipeline"

    - if: $SECURITY_RELEASE_PIPELINE == 'true'
      variables:
        PIPELINE_NAME: "Security pipeline"

    # Pipelines defined in .gitlab/ci/monthly/
    - if: $MONTHLY_RELEASE_PIPELINE == 'finalize'
      variables:
        PIPELINE_NAME: "Monthly release finalize pipeline"

    - if: $MONTHLY_RELEASE_PIPELINE == 'true'
      variables:
        PIPELINE_NAME: "Monthly release pipeline"

    - if: $MONTHLY_RELEASE_METRIC == "announced"
      variables:
        PIPELINE_NAME: "Monthly release metric pipeline for announced status"

    - if: $PRE_DEPLOY == 'true'
      variables:
        PIPELINE_NAME: "Pre deploy pipeline"

    - if: $RELEASE_ENVIRONMENT_PIPELINE == 'true'
      variables:
        PIPELINE_NAME: "Release Environment Pipeline"

    # Always create pipelines for MRs, tags and for default branch
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
